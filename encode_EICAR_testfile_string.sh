#!/usr/bin/env bash
# Author: Pierre Gronau <Pierre.Gronau@ndaal.eu>
# Copyright 2024, 2025
# License: All content is licensed under the terms of the <MIT License>
# Developed on: Debian 12.8x; macOS Sequoia x86 architecture
# Tested on: Debian 12.8x; macOS Sequoia x86 architecture
#
# Note
# This script creates or download files containing the EICAR test string,
# which may trigger antivirus software. Use with caution and only
# in controlled environments for legitimate testing purposes.
#
# 1. Setup and Configuration:
# - Sets strict bash options for error handling and safety.
# - Defines a cleanup function to handle script termination.
# - Defines a Base62 alphabet for encoding.
# 2. Encoding Functions:
# - Implements custom Base62 encoding functions for numbers and strings.
# 3. Python Environment:
# - Locates the Python3 interpreter on the system.
# Main Encoding Process:
# - Defines an EICAR test string as input.
# - Performs various encoding operations on this input string:
#   - a. Base32 encoding (using system command)
#   - b. Base64 encoding (using system command)
#   - c. Custom Base62 encoding (using bash functions)
#   - d. If Python is available, it also performs:
#        - ASCII85 encoding
#        - Base32 encoding
#        - Base45 encoding
#        - Base58 encoding
#        - Base85 encoding
#        - Base91 encoding
#        - BinHex encoding
#        - MIME encoding
# 5. Output:
# - Each encoded version is saved to a separate file named "EICAR_encoded_[encoding].txt".
# - The script prints status messages for each encoding operation.
# 6. Cleanup and Finalization:
# - Runs the cleanup function.
# - Prints information about the script's name and path.
#
# In essence, this script takes a specific string (the EICAR test file string) 
# and encodes it using multiple encoding schemes, saving each result 
# to a separate file. It's designed to work with both system commands and 
# Python-based encoding methods, providing a comprehensive 
# set of encoded versions of the input string.

# Exit on error. Append "|| true" if you expect an error.
set -o errexit
# This is equivalent to set -e. It causes the script to exit
# immediately if any command exits with a non-zero status.
#
# Exit on error inside any functions or subshells.
set -o errtrace
# This setting ensures that the ERR trap is inherited by shell functions,
# command substitutions, and commands executed in a subshell environment.
#
# Do not allow use of undefined vars. Use ${VAR:-} to use an undefined VAR
set -o nounset
# This is equivalent to set -u. It treats unset variables as
# an error when substituting.
#
# Catch the error in case mysqldump fails (but gzip succeeds) in `mysqldump |gzip`
# https://vaneyckt.io/posts/safer_bash
set -o pipefail
# This setting causes a pipeline to return the exit status of the last command
# in the pipe that returned a non-zero status.
#
# Turn on traces, useful while debugging but commented out by default
# set -o xtrace

# Set $IFS to only newline and tab.
#
# http://www.dwheeler.com/essays/filenames-in-shell.html
# nosemgrep: ifs-tampering
IFS=$'\n\t'

# Error trapping function
# Add this line before the error_handler function
# shellcheck disable=SC2317
error_handler() {
    local line_number="${1}"
    local error_code="${2}"
    local last_command="${BASH_COMMAND}"
    printf "Error: Error occurred in line %s (error code: %s)\n" "${line_number}" "${error_code}"
    printf "Error: Failed command: %s\n" "${last_command}"

    # Success case
    return 0
}

# Set the error trap
trap "error_handler ${LINENO} \$?" ERR

cleanup() {
    trap - SIGINT SIGTERM ERR EXIT
    printf "%b\n" "\nInfo: Cleanup is running ..."
    # Additional cleanup tasks can be added here if needed
    rm -f -v ./*.tmp
    rm -f -v "./EICAR_encoded_"*
    printf "%b\n" "\nInfo: Cleanup finished ..."
}

trap cleanup SIGINT SIGTERM ERR EXIT

# Define the EICAR test string
# shellcheck disable=SC2016
input_string='X5O!P%@AP[4\PZX54(P^)7CC)7}$EICAR-STANDARD-ANTIVIRUS-TEST-FILE!$H+H*'

eicar_file="eicar.txt"
readonly eicar_file

printf "%s\n" "${input_string}" > "${eicar_file}"

if [ ! -f "${eicar_file}" ]; then
    printf "Error: %s not found. Exiting.\n" "${eicar_file}"
    exit 1
fi

# Define script path and output directory
script_path="${BASH_SOURCE[0]}"
readonly script_path
printf "Info: Current script_path: %s\n" "${script_path}"

script_dir="$(cd "$(dirname "${script_path}")" && pwd)"
readonly script_dir
printf "Info: Current script_dir: %s\n" "${script_dir}"

output_dir="${script_dir}/dataset/EICAR_original_testfiles"
readonly output_dir
printf "Info: Current output_dir: %s\n" "${output_dir}"

# Create output directory if it doesn't exist
if [ ! -d "${output_dir}" ]; then
    if mkdir "${output_dir}"; then
        printf "Created output directory: %s\n" "${output_dir}"
    else
        printf "Error: Failed to create output directory: %s\n" "${output_dir}" >&2
        exit 1
    fi
else
    printf "Output directory already exists: %s\n" "${output_dir}"
fi

ALPHABET="0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
readonly ALPHABET

encode_number_base62() {
    local num="$1"
    local base62=""
    local base="${#ALPHABET}"
    
    if [ "${num}" -eq 0 ]; then
        base62="${ALPHABET:0:1}"
    else
        while [ "${num}" -gt 0 ]; do
            local rem=$((num % base))
            base62="${ALPHABET:rem:1}${base62}"
            num=$((num / base))
        done
    fi
    
    printf "%s" "${base62}"
}

encode_string_base62() {
    local input_string="$1"
    local base62_encoded=""
    local ascii_value
    local encoded_char
    
    for (( i=0; i<${#input_string}; i++ )); do
        local char="${input_string:i:1}"
        ascii_value="$(printf "%d" "'${char}")"
        encoded_char="$(encode_number_base62 "${ascii_value}")"
        
        while [ ${#encoded_char} -lt 2 ]; do
            encoded_char="0${encoded_char}"
        done
        
        base62_encoded="${base62_encoded}${encoded_char}"
    done
    
    printf "%s" "${base62_encoded}"
}

PYTHON_PATH="$(command -v python3)"

encode_and_save() {
    local input_string="${1}"
    local encoding="${2}"
    local command="${3}"
    local output_file="EICAR_encoded_${encoding}.txt"

    printf "%s" "${input_string}" | eval "${command}" > "${output_file}"
    printf "Encoded using %s and saved to %s\n" "${encoding}" "${output_file}"
}

check_python_module() {
    "${PYTHON_PATH}" -c "import ${1}" 2>/dev/null
    return $?
}

encode_and_save "${input_string}" "base32" "base32"
encode_and_save "${input_string}" "base64" "base64"

base62_encoded="$(encode_string_base62 "${input_string}")"
printf "%s" "${base62_encoded}" > "EICAR_encoded_base62.txt"
printf "Encoded using base62 and saved to EICAR_encoded_base62.txt\n"

if [ -n "${PYTHON_PATH}" ]; then
    if check_python_module "base58"; then
        encode_and_save "${input_string}" "base58" "${PYTHON_PATH} -c 'import sys, base58; print(base58.b58encode(sys.stdin.buffer.read()).decode(), end=\"\")'"
    else
        printf "Python module 'base58' not found. Please install it with: %s -m pip install base58\n" "${PYTHON_PATH}"
    fi

    encode_and_save "${input_string}" "ascii85" "${PYTHON_PATH} -c 'import sys, base64; print(base64.a85encode(sys.stdin.buffer.read()).decode(), end=\"\")'"

    if check_python_module "base45"; then
        encode_and_save "${input_string}" "base45" "${PYTHON_PATH} -c 'import sys, base45; print(base45.b45encode(sys.stdin.buffer.read()).decode(), end=\"\")'"
    else
        printf "Python module 'base45' not found. Please install it with: %s -m pip install base45\n" "${PYTHON_PATH}"
    fi

    if check_python_module "base91"; then
        encode_and_save "${input_string}" "base91" "${PYTHON_PATH} -c 'import sys, base91; print(base91.encode(sys.stdin.buffer.read()), end=\"\")'"
    else
        printf "Python module 'base91' not found. Please install it with: %s -m pip install base91\n" "${PYTHON_PATH}"
    fi

    encode_and_save "${input_string}" "binhex" "${PYTHON_PATH} -c 'import sys, binascii; print(binascii.b2a_hex(sys.stdin.buffer.read()).decode(), end=\"\")'"
    encode_and_save "${input_string}" "base85" "${PYTHON_PATH} -c 'import sys, base64; print(base64.b85encode(sys.stdin.buffer.read()).decode(), end=\"\")'"
    encode_and_save "${input_string}" "mime" "${PYTHON_PATH} -c 'import sys, email.mime.text; print(email.mime.text.MIMEText(sys.stdin.read()).as_string(), end=\"\")'"

    # Custom Base64 Variants
    encode_and_save "${input_string}" "uuencode" "${PYTHON_PATH} -c 'import sys, base64; print(base64.b64encode(sys.stdin.buffer.read(), altchars=b\" !\").decode(), end=\"\")'"
    encode_and_save "${input_string}" "binhex" "${PYTHON_PATH} -c 'import sys, base64; print(base64.b64encode(sys.stdin.buffer.read(), altchars=b\"#$\").decode(), end=\"\")'"
    encode_and_save "${input_string}" "crypt" "${PYTHON_PATH} -c 'import sys, base64; print(base64.b64encode(sys.stdin.buffer.read(), altchars=b\"./\").decode(), end=\"\")'"
    encode_and_save "${input_string}" "bcrypt" "${PYTHON_PATH} -c 'import sys, base64; print(base64.b64encode(sys.stdin.buffer.read(), altchars=b\"./\").decode(), end=\"\")'"
    encode_and_save "${input_string}" "xxencode" "${PYTHON_PATH} -c 'import sys, base64; print(base64.b64encode(sys.stdin.buffer.read(), altchars=b\"+-\").decode(), end=\"\")'"
    encode_and_save "${input_string}" "bash" "${PYTHON_PATH} -c 'import sys, base64; print(base64.b64encode(sys.stdin.buffer.read(), altchars=b\"@_\").decode(), end=\"\")'"

    # Additional Encoders
    encode_and_save "${input_string}" "big5" "${PYTHON_PATH} -c 'import sys; print(sys.stdin.read().encode(\"big5\").hex(), end=\"\")'"
    encode_and_save "${input_string}" "big5hkscs" "${PYTHON_PATH} -c 'import sys; print(sys.stdin.read().encode(\"big5hkscs\").hex(), end=\"\")'"
    encode_and_save "${input_string}" "cp037" "${PYTHON_PATH} -c 'import sys; print(sys.stdin.read().encode(\"cp037\").hex(), end=\"\")'"
    encode_and_save "${input_string}" "cp273" "${PYTHON_PATH} -c 'import sys; print(sys.stdin.read().encode(\"cp273\").hex(), end=\"\")'"
    
    # Split input for IDNA encoding
    idna_encoded=""
    for (( i=0; i<${#input_string}; i+=63 )); do
        segment="${input_string:i:63}"
        segment_encoded="$(${PYTHON_PATH} -c "import sys; print(r'${segment}'.encode('idna').decode('ascii'), end='')")"
        idna_encoded="${idna_encoded}${segment_encoded}"
    done
    printf "%s" "${idna_encoded}" > "EICAR_encoded_idna.txt"
    printf "Info: Encoded using idna and saved to EICAR_encoded_idna.txt\n"

    # Skip mbcs encoding if not on Windows
    if [[ "$(uname -s)" == "MINGW"* || "$(uname -s)" == "CYGWIN"* || "$(uname -s)" == "MSYS"* ]]; then
        encode_and_save "${input_string}" "mbcs" "${PYTHON_PATH} -c 'import sys; print(sys.stdin.read().encode(\"mbcs\").hex(), end=\"\")'"
    else
        printf "Info: MBCS encoding is not supported on this platform. Skipping.\n"
    fi

    encode_and_save "${input_string}" "palmos" "${PYTHON_PATH} -c 'import sys; print(sys.stdin.read().encode(\"palmos\").hex(), end=\"\")'"
    encode_and_save "${input_string}" "punycode" "${PYTHON_PATH} -c 'import sys; print(sys.stdin.read().encode(\"punycode\").decode(\"ascii\"), end=\"\")'"

else
    printf "Error: Python3 not found. Skipping Python-based encodings.\n"
fi

mv -v "./EICAR_encoded_"* "${output_dir}"

printf "Info: Encoding process completed.\n"

cleanup

script_name1="$(basename "${0}")"
printf "\nInfo: script_name1: %s\n" "${script_name1}"
script_path1="$(realpath "$(dirname "${0}")")"
printf "Info: script_path1: %s\n" "${script_path1}"
script_path_with_name="${script_path1}/${script_name1}"
printf "Info: Script path with name: %s\n" "${script_path_with_name}"
printf "Info: Script finished\n"
exit 0
