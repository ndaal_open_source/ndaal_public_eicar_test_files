# robots.txt
# This file is to prevent the crawling and indexing of certain parts
# of your site by web crawlers and spiders run by sites like Yahoo!
# and Google. By telling these "robots" where not to go on your site,
# you save bandwidth and server resources.
#
# https://www.searchenginejournal.com/robots-txt-security-risks/289719/
#
# Security wise, robots.txt usage has two rules.
#
# Do not try to implement any security through robots.txt. The robots file is
# nothing more than a kind suggestion, and while most search engine crawlers 
# respect it, malicious crawlers have a good laugh and continue at their 
# business. If it's linked to, it can be found.
#
# Do not expose interesting information through robots.txt. Specifically,
# if you rely on the URL to control access to certain resources (which is a
# huge alarm bell by itself), adding it to robots.txt will only make the 
# problem worse: an attacker who scans robots.txt will now see the secret URL
# you were trying to hide, and concentrate efforts on that part of your site
# (you don't want it indexed, and it's named 
# 'sekrit-admin-part-do-not-tell-anyone', so it's probably interesting).

# Directories
User-agent: *
Disallow: /
Disallow: /_static/
Disallow: /_branding/
Disallow: /_test/
Disallow: /_example/
Disallow: /_abstract/
Disallow: /_Definition/
Disallow: /_glossary/
Disallow: /_document/
Disallow: /_Compliance/
Disallow: /_Container/
Disallow: /_document/
Disallow: /_Documentation/
Disallow: /_FAQs/
Disallow: /_Guides/
Disallow: /_Pantagrule/
Disallow: /_Pipelines/
Disallow: /_Privacy/
Disallow: /_Security/
Disallow: /_Sigma/
Disallow: /_YARA/
Disallow: /_Cryptography/
Disallow: /_exchange/
Disallow: /_legal/
Disallow: /_log/
Disallow: /_Malware/

# WordPress
User-Agent: *
Disallow: /extend/themes/search.php
Disallow: /themes/search.php
Disallow: /support/rss
Disallow: /archive/
Disallow: /wp-content/plugins/
Disallow: /wp-admin/ <— Check this to confirm a WordPress installation

# Files

# Honeybot
Disallow: /secure/logins.html

# Specific bot blocking

User-agent: ChatGPT-User  
Disallow: /

User-agent: GPTBot  
Disallow: /

User-agent: Google-Extended  
Disallow: /

User-agent: Applebot-Extended  
Disallow: /

User-agent: anthropic-ai  
Disallow: /

User-agent: ClaudeBot  
Disallow: /

User-agent: Omgilibot  
Disallow: /

User-agent: Omgili  
Disallow: /

User-agent: FacebookBot  
Disallow: /

User-agent: Diffbot  
Disallow: /

User-agent: Bytespider  
Disallow: /

User-agent: ImagesiftBot  
Disallow: /

User-agent: PerplexityBot  
Disallow: /

User-agent: cohere-ai  
Disallow: /

User-agent: Meta-ExternalAgent  
Disallow: /

User-agent: Meta-ExternalFetcher  
Disallow: /

User-agent: Timpibot  
Disallow: /

User-agent: Webzio-Extended  
Disallow: /

User-agent: YouBot  
Disallow: /

Sitemap: https://www.ndaal.eu/sitemap.xml
 
