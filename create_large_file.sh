#!/usr/bin/env bash
# Author: Pierre Gronau <Pierre.Gronau@ndaal.eu>
# Copyright 2024,2025
# License: All content is licensed under the terms of the <MIT License>
# Developed on: Debian 12.8x; macOS Sequoia x86 architecture
# Tested on: Debian 12.8x; macOS Sequoia x86 architecture
#
# Exit on error. Append "|| true" if you expect an error.
set -o errexit
# This is equivalent to set -e. It causes the script to exit
# immediately if any command exits with a non-zero status.
#
# Exit on error inside any functions or subshells.
set -o errtrace
# This setting ensures that the ERR trap is inherited by shell functions,
# command substitutions, and commands executed in a subshell environment.
#
# Do not allow use of undefined vars. Use ${VAR:-} to use an undefined VAR
set -o nounset
# This is equivalent to set -u. It treats unset variables as
# an error when substituting.
#
# Catch the error in case mysqldump fails (but gzip succeeds) in `mysqldump |gzip`
# https://vaneyckt.io/posts/safer_bash
set -o pipefail
# This setting causes a pipeline to return the exit status of the last command
# in the pipe that returned a non-zero status.
#
# Turn on traces, useful while debugging but commented out by default
# set -o xtrace

# Set $IFS to only newline and tab.
#
# http://www.dwheeler.com/essays/filenames-in-shell.html
# nosemgrep: ifs-tampering
IFS=$'\n\t'

# Description: This script checks for sufficient disk space and creates a 2500 MB file.
# The following commands all report the same error when running on a Windows system:
#
# clamscan.exe -v -a --stdout  --max-filesize=1000M --max-scansize=1000M --alert-exceeds-max=yes "C:\Users\me\Documents\2500M.file"
#
# clamscan.exe -v -a --stdout  --max-filesize=1000M --max-scansize=1000M --alert-exceeds-max=no "C:\Users\me\Documents\2500M.file"
#
# clamscan.exe -v -a --stdout  --max-filesize=0 --max-scansize=0 --alert-exceeds-max=yes "C:\Users\me\Documents\2500M.file"
# https://github.com/Cisco-Talos/clamav/issues/1315

# Error trapping function
# Add this line before the error_handler function
# shellcheck disable=SC2317
error_handler() {
    local line_number="${1}"
    local error_code="${2}"
    local last_command="${BASH_COMMAND}"
    printf "Error: Error occurred in line %s (error code: %s)\n" "${line_number}" "${error_code}"
    printf "Error: Failed command: %s\n" "${last_command}"

    # Success case
    return 0
}

# Set the error trap
trap "error_handler ${LINENO} \$?" ERR

cleanup() {
    trap - SIGINT SIGTERM ERR EXIT
    printf "%b\n" "\nInfo: Cleanup is running ..."
    # Additional cleanup tasks can be added here if needed
    rm -f -v ./*.tmp
    printf "%b\n" "\nInfo: Cleanup finished ..."
}

trap cleanup SIGINT SIGTERM EXIT

# Set the desired file size in MB
FILE_SIZE_MB=2250
readonly FILE_SIZE_MB
FILE_NAME="large_file.bin"
readonly FILE_NAME

# Function to remove the file if it exists
remove_file() {
    if [ -e "${FILE_NAME}" ]; then  # Check if the file exists
        if rm -f -v "${FILE_NAME}"; then
            printf "Warning: Successfully removed the file: '%s'\n" "${FILE_NAME}"
        else
            printf "Error: Failed to remove the file: '%s'\n" "${FILE_NAME}"
        fi
    else
        printf "Info: File '%s' does not exist.\n" "${FILE_NAME}"
    fi
}

# Function to check available disk space
check_disk_space() {
    local required_space=$((FILE_SIZE_MB * 1024 * 1024)) # Convert MB to bytes
    readonly required_space
    local available_space

    # Get available space in bytes using df and awk
    available_space=$(df . | awk 'NR==2 {print $4 * 1024}') # Get available space in bytes
    if [ "${available_space}" -lt "${required_space}" ]; then
        printf "Error: Not enough disk space. Required: %d MB, Available: %d MB.\n" "${FILE_SIZE_MB}" "$((available_space / 1024 / 1024))"
        exit 1
    fi
}

# Function to create the file
create_large_file() {
    # Create a 2500 MB file with /dev/zero
    dd if=/dev/zero of="${FILE_NAME}" bs=1M count="${FILE_SIZE_MB}" status=progress
    printf "Info: File '%s' of size %d MB created successfully.\n" "${FILE_NAME}" "${FILE_SIZE_MB}"
}

# Main script execution
remove_file
check_disk_space
create_large_file

printf "\nInfo: Processing complete.\n"

cleanup

script_name1="$(basename "${0}")"
printf "\nInfo: script_name1: %s\n" "${script_name1}"
script_path1="$(realpath "$(dirname "${0}")")"
printf "Info: script_path1: %s\n" "${script_path1}"
script_path_with_name="${script_path1}/${script_name1}"
printf "Info: Script path with name: %s\n" "${script_path_with_name}"
printf "Info: Script finished\n"
exit 0
