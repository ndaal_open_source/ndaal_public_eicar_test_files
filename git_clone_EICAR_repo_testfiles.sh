#!/usr/bin/env bash
# Author: Pierre Gronau <Pierre.Gronau@ndaal.eu>
# Copyright 2024, 2025
# License: All content is licensed under the terms of the <MIT License>
# Developed on: Debian 12.8x; macOS Sequoia x86 architecture
# Tested on: Debian 12.8x; macOS Sequoia x86 architecture
#
# Note
# This script creates or download files containing the EICAR test string,
# which may trigger antivirus software. Use with caution and only
# in controlled environments for legitimate testing purposes.
#
# Exit on error. Append "|| true" if you expect an error.
set -o errexit
# This is equivalent to set -e. It causes the script to exit
# immediately if any command exits with a non-zero status.
#
# Exit on error inside any functions or subshells.
set -o errtrace
# This setting ensures that the ERR trap is inherited by shell functions,
# command substitutions, and commands executed in a subshell environment.
#
# Do not allow use of undefined vars. Use ${VAR:-} to use an undefined VAR
set -o nounset
# This is equivalent to set -u. It treats unset variables as
# an error when substituting.
#
# Catch the error in case mysqldump fails (but gzip succeeds) in `mysqldump |gzip`
# https://vaneyckt.io/posts/safer_bash
set -o pipefail
# This setting causes a pipeline to return the exit status of the last command
# in the pipe that returned a non-zero status.
#
# Turn on traces, useful while debugging but commented out by default
# set -o xtrace

# Set $IFS to only newline and tab.
#
# http://www.dwheeler.com/essays/filenames-in-shell.html
# nosemgrep: ifs-tampering
IFS=$'\n\t'

# Error trapping function
# Add this line before the error_handler function
# shellcheck disable=SC2317
error_handler() {
    local line_number="${1}"
    local error_code="${2}"
    local last_command="${BASH_COMMAND}"
    printf "Error: Error occurred in line %s (error code: %s)\n" "${line_number}" "${error_code}"
    printf "Error: Failed command: %s\n" "${last_command}"

    # Success case
    return 0
}

# Set the error trap
trap "error_handler ${LINENO} \$?" ERR

cleanup() {
    trap - SIGINT SIGTERM ERR EXIT
    printf "%b\n" "\nInfo: Cleanup is running ..."
    # Additional cleanup tasks can be added here if needed
    rm -f -v ./*.tmp
    printf "%b\n" "\nInfo: Cleanup finished ..."
}

trap cleanup SIGINT SIGTERM EXIT

# Define SCRIPT_PATH
SCRIPT_PATH=$(dirname "$(realpath "${0}")")
readonly SCRIPT_PATH
printf "Info: Current SCRIPT_PATH: %s\n" "${SCRIPT_PATH}"

DATASET_PATH="${SCRIPT_PATH}/dataset"
readonly DATASET_PATH
printf "Info: Current DATASET_PATH: %s\n" "${DATASET_PATH}"

# Create an array with Git repository URLs
declare -ar git_repos=(
    "https://github.com/dwestgard/archive_samples_eicar"
    "https://github.com/fire1ce/eicar-standard-antivirus-test-files"
    "https://github.com/alos-source/EICAR-TEST-FILES"
    "https://github.com/popalltheshells/EICARsandboxCheck"
    "https://github.com/mattias-ohlsson/eicar-standard-antivirus-test-files"
    "https://github.com/wdhif/docker-eicar"
    "https://github.com/mauricelambert/EicarSpam"
)

# Function to check if Git is available
check_git() {
    if ! command -v git &> /dev/null; then
        printf "Error: Git is not installed or not in PATH.\n" >&2
        exit 1
    fi
}

# Function to create dataset directory if it doesn't exist
create_dataset_dir() {
    if [ ! -d "${DATASET_PATH}" ]; then
        mkdir -p "${DATASET_PATH}"
        printf "Created dataset directory: %s\n" "${DATASET_PATH}"
    fi
}

# Function to clone or update a Git repository
clone_or_update_repo() {
    local repo_url="${1}"
    local repo_name
    repo_name=$(basename "${repo_url}" .git)
    local repo_path="${DATASET_PATH}/${repo_name}"

    if [ -d "${repo_path}" ]; then
        printf "Updating repository: %s\n" "${repo_name}"
        git -C "${repo_path}" fetch --all
        git -C "${repo_path}" reset --hard origin/main || git -C "${repo_path}" reset --hard origin/master
        git -C "${repo_path}" pull
    else
        printf "Cloning repository: %s\n" "${repo_name}"
        git clone "${repo_url}" "${repo_path}"
    fi

    # Checkout the latest commit
    git -C "${repo_path}" checkout "$(git -C "${repo_path}" rev-parse HEAD)"
}

# Function to remove .git directories
remove_git_dirs() {
    local repo_url="${1}"
    local repo_name
    repo_name=$(basename "${repo_url}" .git)
    local repo_path="${DATASET_PATH}/${repo_name}"
    local git_dir="${repo_path}/.git"

    if [ -d "${git_dir}" ]; then
        printf "Removing .git directory from: %s\n" "${repo_name}"
        rm -rf "${git_dir}"
        if [ $? -eq 0 ]; then
            printf "Successfully removed .git directory from: %s\n" "${repo_name}"
        else
            printf "Failed to remove .git directory from: %s\n" "${repo_name}"
        fi
    else
        printf "No .git directory found in: %s\n" "${repo_name}"
    fi
}


# Main execution
main() {
    check_git
    create_dataset_dir

    for repo in "${git_repos[@]}"; do
        if clone_or_update_repo "${repo}"; then
            printf "Successfully processed repository: %s\n" "${repo}"
            remove_git_dirs "${repo}"
        else
            printf "Failed to process repository: %s\n" "${repo}"
        fi
    done
}



main

printf "\nInfo: Processing complete.\n"

cleanup

script_name1="$(basename "${0}")"
printf "\nInfo: script_name1: %s\n" "${script_name1}"
script_path1="$(realpath "$(dirname "${0}")")"
printf "Info: script_path1: %s\n" "${script_path1}"
script_path_with_name="${script_path1}/${script_name1}"
printf "Info: Script path with name: %s\n" "${script_path_with_name}"
printf "Info: Script finished\n"
exit 0
