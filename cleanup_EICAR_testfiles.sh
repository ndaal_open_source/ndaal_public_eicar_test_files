#!/usr/bin/env bash
# Author: Pierre Gronau <Pierre.Gronau@ndaal.eu>
# Copyright 2024, 2025
# License: All content is licensed under the terms of the <MIT License>
# Developed on: Debian 12.7x; macOS Sequoia x86 architecture
# Tested on: Debian 12.7x; macOS Sequoia x86 architecture
#
# Note
# This script creates or download files containing the EICAR test string,
# which may trigger antivirus software. Use with caution and only
# in controlled environments for legitimate testing purposes.
#
# Exit on error. Append "|| true" if you expect an error.
set -o errexit
# This is equivalent to set -e. It causes the script to exit
# immediately if any command exits with a non-zero status.
#
# Exit on error inside any functions or subshells.
set -o errtrace
# This setting ensures that the ERR trap is inherited by shell functions,
# command substitutions, and commands executed in a subshell environment.
#
# Do not allow use of undefined vars. Use ${VAR:-} to use an undefined VAR
set -o nounset
# This is equivalent to set -u. It treats unset variables as
# an error when substituting.
#
# Catch the error in case mysqldump fails (but gzip succeeds) in `mysqldump |gzip`
# https://vaneyckt.io/posts/safer_bash
set -o pipefail
# This setting causes a pipeline to return the exit status of the last command
# in the pipe that returned a non-zero status.
#
# Turn on traces, useful while debugging but commented out by default
# set -o xtrace

# Set $IFS to only newline and tab.
#
# http://www.dwheeler.com/essays/filenames-in-shell.html
# nosemgrep: ifs-tampering
IFS=$'\n\t'

# Error trapping function
# Add this line before the error_handler function
# shellcheck disable=SC2317
error_handler() {
    local line_number="${1}"
    local error_code="${2}"
    local last_command="${BASH_COMMAND}"
    printf "Error: Error occurred in line %s (error code: %s)\n" "${line_number}" "${error_code}"
    printf "Error: Failed command: %s\n" "${last_command}"

    # Success case
    return 0
}

# Set the error trap
trap "error_handler ${LINENO} \$?" ERR

cleanup() {
    trap - SIGINT SIGTERM ERR EXIT
    printf "%b\n" "\nInfo: Cleanup is running ..."
    # Additional cleanup tasks can be added here if needed
    rm -f -v ./*.tmp
    printf "%b\n" "\nInfo: Cleanup finished ..."
}

trap cleanup SIGINT SIGTERM EXIT

# Define script path and output directory
script_path="${BASH_SOURCE[0]}"
readonly script_path

script_dir="$(cd "$(dirname "${script_path}")" && pwd)"
readonly script_dir

DESTINATION="${script_dir}"
printf "Info: Destination Directory: %s\n" "${DESTINATION}"

# Define an array of file names
readonly file_names=(
  "large_file.bin"
  "NAVAir-native-Creo-TDP.pdf"
  "Native-NX-PMI-TDP-Complete.pdf"
  "veraPDF test suite 6-3-2-t01-pass-a.pdf"
  "nohup.out"
)

# Function to remove files if they exist
remove_files() {
  for file_name in "${file_names[@]}"; do
    if [ -e "${file_name}" ]; then  # Check if the file exists
      if rm -f -v -- "${file_name}"; then
        printf "Info: Successfully removed the file: '%s'\n" "${file_name}"
      else
        printf "Error: Failed to remove the file: '%s'\n" "${file_name}"
      fi
    else
      printf "Warning: File '%s' does not exist.\n" "${file_name}"
    fi
  done

  # Success case
  return 0
}

remove_log_count_files() {
  log_count_files=(log_count_*)
  if [ ${#log_count_files[@]} -eq 0 ] || [ "${log_count_files[0]}" = "log_count_*" ]; then
    printf "Warning: No log_count_* files found.\n"
  else
    for file in "${log_count_files[@]}"; do
      if rm -f -v -- "${file}"; then
        printf "Info: Successfully removed the file: '%s'\n" "${file}"
      else
        printf "Error: Failed to remove the file: '%s'\n" "${file}"
      fi
    done
  fi
}

Function_Remove_Unwished_Files () {
    printf "\nInfo: Remove Unwished Files\n"
    printf "Info: Currently, files with these extensions are considered temporary files:\n"
    printf "%s\n" "Info: thumbs.db, .bak, ~, .tmp, .temp, .DS_Store, .crdownload, .part, .cache, .dmp, .download, .partial,"
    printf "%s\n" "Info: .swp, .log, .old, .$$$, .wbk, .xlk, .~lock, .lck, .err, .chk, .sik, .crash, .temp$, .bup, .save"
    printf "Warning: Some of these files may be important for recovery or debugging.\n"
    printf "Info: Always ensure you have backups before performing any mass deletion of files.\n\n"

    printf "%s\n" "Info: remove .DS_Store (macOS desktop services store)"
    find . -name ".DS_Store" -exec rm -rf {} \; || true
    printf "%s\n" "Info: remove thumbs.db and Thumbs.db (Windows thumbnail cache)"
    find . -name "thumbs.db" -exec rm -rf {} \; || true
    find . -name "Thumbs.db" -exec rm -rf {} \; || true
    printf "%s\n" "Info: remove .crdownload (Chrome download temp files)"
    find . -name ".crdownload" -exec rm -rf {} \; || true
    printf "%s\n" "Info: remove .download (Generic download temp files)"
    find . -name ".download" -exec rm -rf {} \; || true
    printf "%s\n" "Info: remove .partial (Firefox/Mozilla partial download files)"
    find . -name ".partial" -exec rm -rf {} \; || true
    printf "%s\n" "Info: remove .cache (Generic cache files)"
    find . -name ".cache" -exec rm -rf {} \; || true
    printf "%s\n" "Info: remove .dmp (Memory dump files)"
    find . -name ".dmp" -exec rm -rf {} \; || true
    printf "%s\n" "Info: remove .tmp and .temp (Generic temporary files)"
    find . -name ".tmp" -exec rm -rf {} \; || true
    find . -name ".temp" -exec rm -rf {} \; || true
    printf "%s\n" "Info: remove .bak (Generic backup files)"
    find . -name ".bak" -exec rm -rf {} \; || true
    printf "%s\n" "Info: remove .backup (Generic backup files)"
    find . -name ".backup" -exec rm -rf {} \; || true
    printf "%s\n" "Info: remove .log (Log files, use with caution)"
    find . -name ".log" -exec rm -rf {} \; || true
    printf "%s\n" "Info: remove .swp (Vim swap files)"
    find . -name "*.swp" -exec rm -rf {} \; || true
    printf "%s\n" "Info: remove .old (Old version files)"
    find . -name "*.old" -exec rm -rf {} \; || true
    printf "%s\n" "Info: remove .$$$ (Temporary files used by some programs)"
    find . -name "*.$$$" -exec rm -rf {} \; || true
    printf "%s\n" "Info: remove .wbk (Word backup files)"
    find . -name "*.wbk" -exec rm -rf {} \; || true
    printf "%s\n" "Info: remove .xlk (Excel backup files)"
    find . -name "*.xlk" -exec rm -rf {} \; || true
    printf "%s\n" "Info: remove .~lock (LibreOffice lock files)"
    find . -name ".~lock*" -exec rm -rf {} \; || true
    printf "%s\n" "Info: remove .lck (Generic lock files)"
    find . -name "*.lck" -exec rm -rf {} \; || true
    printf "%s\n" "Info: remove .err (Error log files)"
    find . -name "*.err" -exec rm -rf {} \; || true
    printf "%s\n" "Info: remove .chk (Checkpoint files)"
    find . -name "*.chk" -exec rm -rf {} \; || true
    printf "%s\n" "Info: remove .sik (Backup files used by some Adobe products)"
    find . -name "*.sik" -exec rm -rf {} \; || true
    printf "%s\n" "Info: remove .crash (Crash dump files)"
    find . -name "*.crash" -exec rm -rf {} \; || true
    printf "%s\n" "Info: remove .temp$ (Another variant of temp files)"
    find . -name "*.temp$" -exec rm -rf {} \; || true
    printf "%s\n" "Info: remove .bup (Backup files)"
    find . -name "*.bup" -exec rm -rf {} \; || true
    printf "%s\n" "Info: remove .save (Saved versions of files)"
    find . -name "*.save" -exec rm -rf {} \; || true

    printf "\nInfo: Removal process completed. Please review any error messages carefully.\n"

    # Success case
    return 0
}

Function_Create_Checksums_for_Scripts() {
    printf '%s\n' "Info: Create checksums files"
    printf '%s\n' "Warning: Some of these files may be important for verification or integrity checks."
    printf '%s\n' "Info: Always ensure you have backups before performing any mass deletion of files."
    printf '%s\n\n' ""

    # Remove .md5 files
    printf '%s\n' "Info: remove .md5 (MD5 hash files)"
    find . -name "*.md5" -print0 | xargs -0 rm -rf || true

    # Remove .sha-3-512 files
    printf '%s\n' "Info: remove .sha-3-512 (SHA-3 512-bit hash files)"
    find . -name "*.sha-3-512" -print0 | xargs -0 rm -rf || true

    # Remove .sha3-512 files
    printf '%s\n' "Info: remove .sha3-512 (SHA-3 512-bit hash files)"
    find . -name "*.sha3-512" -print0 | xargs -0 rm -rf || true

    # Remove .sha512 files
    printf '%s\n' "Info: remove .sha512 (SHA-512 hash files)"
    find . -name "*.sha512" -print0 | xargs -0 rm -rf || true

    # Remove .sha1 files
    printf '%s\n' "Info: remove .sha1 (SHA-1 hash files)"
    find . -name "*.sha1" -print0 | xargs -0 rm -rf || true

    # Remove .sha256 files
    printf '%s\n' "Info: remove .sha256 (SHA-256 hash files)"
    find . -name "*.sha256" -print0 | xargs -0 rm -rf || true

    # Remove .twoxhash files
    printf '%s\n' "Info: remove .twoxhash (TwoX hash files)"
    find . -name "*.twoxhash" -print0 | xargs -0 rm -rf || true

    # Remove .k12 files
    printf '%s\n' "Info: remove .k12 (KangarooTwelve hash files)"
    find . -name "*.k12" -print0 | xargs -0 rm -rf || true

    # Remove .blake2 files
    printf '%s\n' "Info: remove .blake2 (BLAKE2 hash files)"
    find . -name "*.blake2" -print0 | xargs -0 rm -rf || true

    # Remove .blake3 files
    printf '%s\n' "Info: remove .blake3 (BLAKE3 hash files)"
    find . -name "*.blake3" -print0 | xargs -0 rm -rf || true

    # Remove .crc32 files
    printf '%s\n' "Info: remove .crc32 (CRC32 checksum files)"
    find . -name "*.crc32" -print0 | xargs -0 rm -rf || true

    # Create SHA-3 512 checksums for .sh files
    find "${DESTINATION}" -type f -name "*.sh" -print0 |
        xargs -0 -n 1 sh -c "sha512sum \"\$1\" | awk '{print \$1}' | tee \"\$1.sha3-512\"" _

    # Create SHA-3 512 checksums for .cfg files
    find "${DESTINATION}" -type f -name "*.cfg" -print0 |
        xargs -0 -n 1 sh -c "sha512sum \"\$1\" | awk '{print \$1}' | tee \"\$1.sha3-512\"" _

    # Success case
    return 0
}

printf "%s\n" "Info: Cleanup EICAR testfiles."

remove_files
remove_log_count_files
Function_Remove_Unwished_Files
Function_Create_Checksums_for_Scripts

cleanup

script_name1="$(basename "${0}")"
printf "\nInfo: script_name1: %s\n" "${script_name1}"
script_path1="$(realpath "$(dirname "${0}")")"
printf "Info: script_path1: %s\n" "${script_path1}"
script_path_with_name="${script_path1}/${script_name1}"
printf "Info: Script path with name: %s\n" "${script_path_with_name}"
printf "Info: Script finished\n"
exit 0
