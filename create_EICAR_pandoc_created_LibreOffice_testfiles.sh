#!/usr/bin/env bash
# Author: Pierre Gronau <Pierre.Gronau@ndaal.eu>
# Copyright 2024, 2025
# License: All content is licensed under the terms of the <MIT License>
# Developed on: Debian 12.8x; macOS Sequoia x86 architecture
# Tested on: Debian 12.8x; macOS Sequoia x86 architecture
#
# Note
# This script creates or download files containing the EICAR test string,
# which may trigger antivirus software. Use with caution and only
# in controlled environments for legitimate testing purposes.
#
# Exit on error. Append "|| true" if you expect an error.
set -o errexit
# This is equivalent to set -e. It causes the script to exit
# immediately if any command exits with a non-zero status.
#
# Exit on error inside any functions or subshells.
set -o errtrace
# This setting ensures that the ERR trap is inherited by shell functions,
# command substitutions, and commands executed in a subshell environment.
#
# Do not allow use of undefined vars. Use ${VAR:-} to use an undefined VAR
set -o nounset
# This is equivalent to set -u. It treats unset variables as
# an error when substituting.
#
# Catch the error in case mysqldump fails (but gzip succeeds) in `mysqldump |gzip`
# https://vaneyckt.io/posts/safer_bash
set -o pipefail
# This setting causes a pipeline to return the exit status of the last command
# in the pipe that returned a non-zero status.
#
# Turn on traces, useful while debugging but commented out by default
# set -o xtrace

# Set $IFS to only newline and tab.
#
# http://www.dwheeler.com/essays/filenames-in-shell.html
# nosemgrep: ifs-tampering
IFS=$'\n\t'

# Error trapping function
# Add this line before the error_handler function
# shellcheck disable=SC2317
error_handler() {
    local line_number="${1}"
    local error_code="${2}"
    local last_command="${BASH_COMMAND}"
    printf "Error: Error occurred in line %s (error code: %s)\n" "${line_number}" "${error_code}"
    printf "Error: Failed command: %s\n" "${last_command}"

    # Success case
    return 0
}

# Set the error trap
trap "error_handler ${LINENO} \$?" ERR

cleanup() {
    trap - SIGINT SIGTERM ERR EXIT
    printf "%b\n" "\nInfo: Cleanup is running ..."
    # Additional cleanup tasks can be added here if needed
    rm -f -v ./*.tmp
    printf "%b\n" "\nInfo: Cleanup finished ..."
}

trap cleanup SIGINT SIGTERM EXIT

# Define the EICAR test string
# shellcheck disable=SC2016
input_string='X5O!P%@AP[4\PZX54(P^)7CC)7}$EICAR-STANDARD-ANTIVIRUS-TEST-FILE!$H+H*'
readonly input_string

# Define the EICAR file name
eicar_file="eicar.txt"
readonly eicar_file

# Create a text file with the EICAR test string and ensure it ends with a newline
printf "%s\n" "${input_string}" > "${eicar_file}"

if [ ! -f "${eicar_file}" ]; then
    printf "Error: %s not found. Exiting.\n" "${eicar_file}"
    exit 1
fi

# Define script path and output directory
script_path="${BASH_SOURCE[0]}"
readonly script_path
printf "Info: Current script_path: %s\n" "${script_path}"

script_dir="$(cd "$(dirname "${script_path}")" && pwd)"
readonly script_dir
printf "Info: Current script_dir: %s\n" "${script_dir}"

output_dir="${script_dir}/dataset/EICAR_compressed_nested"
readonly output_dir
printf "Info: Current output_dir: %s\n" "${output_dir}"

# Create output directory if it doesn't exist
if [ ! -d "${output_dir}" ]; then
    if mkdir "${output_dir}"; then
        printf "Info: Created output directory: %s\n" "${output_dir}"
    else
        printf "Error: Failed to create output directory: %s\n" "${output_dir}" >&2
        exit 1
    fi
else
    printf "Info: Output directory already exists: %s\n" "${output_dir}"
fi

# Function to create EICAR text file
create_eicar_text_file() {
    local filename="${1}"
    printf "%s\n" "${input_string}" > "${filename}"
    echo "Info: Created ${filename}"
}

# Function to create EICAR CSV files
create_eicar_csv_files() {
    local filename1="${1}"
    local filename2="${2}"
    
    # CSV with EICAR string in all three columns
    printf '"%s","%s","%s"\n' "${input_string}" "${input_string}" "${input_string}" > "${filename1}"
    echo "Info: Created ${filename1}"
    
    # CSV with EICAR string in first column, others empty
    printf '"%s","",""\n' "${input_string}" > "${filename2}"
    echo "Info: Created ${filename2}"
}

# Create EICAR text file
create_eicar_text_file "${output_dir}/eicar.txt"

# Create EICAR CSV files
create_eicar_csv_files "${output_dir}/eicar_full.csv" "${output_dir}/eicar_partial.csv"

# Convert text to ODT using pandoc
pandoc -s "${output_dir}/eicar.txt" -o "${output_dir}/eicar.odt" --metadata title="EICAR Test"
echo "Converted ${output_dir}/eicar.txt to odt"

# Convert CSV files to ODS using LibreOffice (if available)
if command -v libreoffice &> /dev/null; then
    libreoffice --convert-to ods --outdir "${output_dir}" "${output_dir}/eicar_full.csv"
    echo "Converted ${output_dir}/eicar_full.csv to ods using LibreOffice"
    
    libreoffice --convert-to ods --outdir "${output_dir}" "${output_dir}/eicar_partial.csv"
    echo "Converted ${output_dir}/eicar_partial.csv to ods using LibreOffice"
else
    echo "LibreOffice not found. Skipping CSV to ODS conversion."
    # Fallback to creating simple ODS files
    for csv_file in "${output_dir}/eicar_full.csv" "${output_dir}/eicar_partial.csv"; do
        ods_file="${csv_file%.csv}.ods"
        echo '<?xml version="1.0" encoding="UTF-8"?>
<office:document-content xmlns:office="urn:oasis:names:tc:opendocument:xmlns:office:1.0">
  <office:body>
    <office:spreadsheet>
      <table:table>
        <table:table-row>
          <table:table-cell office:value-type="string">
            <text:p>'"$(cat "${csv_file}")"'</text:p>
          </table:table-cell>
        </table:table-row>
      </table:table>
    </office:spreadsheet>
  </office:body>
</office:document-content>' > "${ods_file}"
        echo "Created a simple ODS file for ${csv_file}"
    done
fi

printf "Info: All files have been created and copied in %s.\n" "${output_dir}"

cleanup

script_name1="$(basename "${0}")"
printf "\nInfo: script_name1: %s\n" "${script_name1}"
script_path1="$(realpath "$(dirname "${0}")")"
printf "Info: script_path1: %s\n" "${script_path1}"
script_path_with_name="${script_path1}/${script_name1}"
printf "Info: Script path with name: %s\n" "${script_path_with_name}"
printf "Info: Script finished\n"
exit 0
