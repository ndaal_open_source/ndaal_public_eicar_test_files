#!/usr/bin/env bash
# Author: Pierre Gronau <Pierre.Gronau@ndaal.eu>
# Copyright 2024, 2025
# License: All content is licensed under the terms of the <MIT License>
# Developed on: Debian 12.9x; macOS Sequoia x86 architecture
# Tested on: Debian 12.9x; macOS Sequoia x86 architecture
#
# Note
# This script creates or download files containing the EICAR test string, 
# which may trigger antivirus software. Use with caution and only 
# in controlled environments for legitimate testing purposes.
#
# Exit on error. Append "|| true" if you expect an error.
set -o errexit
# This is equivalent to set -e. It causes the script to exit 
# immediately if any command exits with a non-zero status.
#
# Exit on error inside any functions or subshells.
set -o errtrace
# This setting ensures that the ERR trap is inherited by shell functions,
# command substitutions, and commands executed in a subshell environment.
#
# Do not allow use of undefined vars. Use ${VAR:-} to use an undefined VAR
set -o nounset
# This is equivalent to set -u. It treats unset variables as 
# an error when substituting.
#
# Catch the error in case mysqldump fails (but gzip succeeds) in `mysqldump |gzip`
# https://vaneyckt.io/posts/safer_bash
#set -o pipefail
# This setting causes a pipeline to return the exit status of the last command
# in the pipe that returned a non-zero status.
#
# Turn on traces, useful while debugging but commented out by default
# set -o xtrace

# Set $IFS to only newline and tab.
#
# http://www.dwheeler.com/essays/filenames-in-shell.html
# nosemgrep: ifs-tampering
IFS=$'\n\t'

# Error trapping function
# Add this line before the error_handler function
# shellcheck disable=SC2317
error_handler() {
    local line_number="${1}"
    local error_code="${2}"
    local last_command="${BASH_COMMAND}"
    printf "Error: Error occurred in line %s (error code: %s)\n" "${line_number}" "${error_code}"
    printf "Error: Failed command: %s\n" "${last_command}"

    # Success case
    return 0
}

# Set the error trap
trap "error_handler ${LINENO} \$?" ERR

cleanup() {
    trap - SIGINT SIGTERM ERR EXIT
	printf "%b\n" "\nInfo: Cleanup is running ..."
	append_timestamp_to_log "Info: Cleanup is running ..."
	# Additional cleanup tasks can be added here if needed
	append_timestamp_to_log "Info: manage_log_copies is running ..."
	manage_log_copies
	append_timestamp_to_log "Info: manage_log_copies is completed."
	printf "%b\n" "\nInfo: Cleanup finished ..."
}

trap cleanup SIGINT SIGTERM EXIT

# Define the debug variable
debug=1 # Set this to 0 or 1 as needed
printf "Info: Debug mode is: %s\n" "${debug}"

# Constants initial
DIRDATE="$(date +"%Y-%m-%d")"
readonly DIRDATE
printf "Info: Current date: %s\n" "${DIRDATE}"

SCRIPT_PATH=$(dirname "${0}")
SCRIPT_PATH=$(realpath "${SCRIPT_PATH}")
readonly SCRIPT_PATH
printf "Info: SCRIPT_PATH: %s\n" "${SCRIPT_PATH}"

SCRIPT_NAME=$(basename "$0")
readonly SCRIPT_NAME
printf "Info: SCRIPT_NAME: %s\n" "${SCRIPT_NAME}"

# Get the current date and time in the desired format
LOGTIME="$(date +"%Y-%m-%d_%H-%M-%S")"
printf "Info: Current date and time: %s\n" "${LOGTIME}"

LOG_COUNT="${SCRIPT_PATH}/log_count_${DIRDATE}.txt"
readonly LOG_COUNT
printf "Info: Log file is: %s\n" "${LOG_COUNT}"

printf "Warning: Delete old log file: %s\n" "${LOG_COUNT}"
rm -f -v "${LOG_COUNT}"

# Define the logging function
append_timestamp_to_log() {
	local message="${1}"
	if [[ ${debug} -eq 1 ]]; then
		LOGTIME="$(date +"%Y-%m-%d_%H-%M-%S")"
		printf "[%s] %s\n" "${LOGTIME}" "${message}"
		printf "[%s] %s\n" "${LOGTIME}" "${message}" >>"${LOG_COUNT}"
	fi

	# Success case
	return 0
}

append_timestamp_to_log "Info: Script started"

append_timestamp_to_log "Define function to check debug value and exit if invalid."
# Function to check debug value and exit if invalid
check_debug_value() {
	if [[ ${debug} != 0 && ${debug} != 1 ]]; then
		printf "Error: Invalid debug value. Must be 0 or 1.\n" >&2
		append_timestamp_to_log "Error: Invalid debug value. Must be 0 or 1."
		return 1
	fi

	# Success case
	return 0
}

append_timestamp_to_log "Info: Call the debug value check function"
# Call the debug value check function
check_debug_value

# Define the EICAR test string
# shellcheck disable=SC2016
input_string='X5O!P%@AP[4\PZX54(P^)7CC)7}$EICAR-STANDARD-ANTIVIRUS-TEST-FILE!$H+H*'
readonly input_string

printf "Info: EICAR test string: %s\n" "${input_string}"

append_timestamp_to_log "Info: Current date: ${DIRDATE}"
append_timestamp_to_log "Info: ${SCRIPT_PATH}"
append_timestamp_to_log "Info: Debug mode is ${debug}"

DESTINATION="${SCRIPT_PATH}/dataset"
readonly DESTINATION
printf "Info: Current DESTINATION: %s\n" "${DESTINATION}"

append_timestamp_to_log "Info: Check if the destination directory exists"
# Check if the destination directory exists
if [ ! -d "${DESTINATION}" ]; then
    printf "Error: Destination directory %s does not exist.\n" "${DESTINATION}" >&2
    exit 1
fi

append_timestamp_to_log "Define function to manage log copies"
# Function to manage log copies
manage_log_copies() {
	printf "Info: manage_log_copies function called.\n"
	append_timestamp_to_log "Info: manage_log_copies function called."

	local base_log="${LOG_COUNT}"
	printf "Info: base_log is %s.\n" "${LOG_COUNT}"
	local max_copies=5 # We keep 5 copies plus the original, so 6 in total.
	printf "Info: max_copies is %d. We keep 5 copies plus the original, so 6 in total.\n" "${max_copies}"
	append_timestamp_to_log "Info: max_copies is ${max_copies}. We keep 5 copies plus the original, so 6 in total.\n"

	# Create a new copy with a timestamp
	local timestamp
	timestamp=$(date +"%Y-%m-%d_%H-%M-%S")
	local new_copy="${base_log}.${timestamp}"
	cp -f -p -v "${base_log}" "${new_copy}"

	# Get all copies sorted by modification time (oldest first)
	local copies
	mapfile -t copies < <(ls -t "${base_log}"*)

	# If we have more than max_copies + 1 (including the original), delete the oldest
	if [ ${#copies[@]} -gt $((max_copies + 1)) ]; then
		local oldest="${copies[-1]}"
		if [ "${oldest}" != "${base_log}" ]; then
			rm -f -v "${oldest}"
			printf "Warning: Deleted oldest log copy: %s\n" "${oldest}"
			append_timestamp_to_log "Warning: Deleted oldest log copy: ${oldest}"
		fi
	fi

	printf "Info: Created new log copy: %s\n" "${new_copy}"
	append_timestamp_to_log "Info: Created new log copy: ${new_copy}"

	# Success case
	return 0
}

append_timestamp_to_log "Define function to check if a command is available."

# Function to check if a command is available
check_command() {
    if ! command -v "${1}" &>/dev/null; then
        printf "Error: %s is not installed or not in PATH. Please install it and try again.\n" "${1}"
        return 1
    fi

    # Success case
    return 0
}

# Check for required commands
check_command "rg" || exit 1

append_timestamp_to_log "Define function to check if a command is available."
# Function to check if a command is available
check_command() {
	if ! command -v "${1}" &>/dev/null; then
		printf "Error: %s is not installed or not in PATH. Please install it and try again.\n" "${1}"
		append_timestamp_to_log "Error: %s is not installed or not in PATH. Please install it and try again."
		return 1
	fi

	append_timestamp_to_log "Info: Command ${1} is available."
	# Success case
	return 0
}

append_timestamp_to_log "Info: Check for required commands"
# Check for required commands
check_command "rg" || exit 1
check_command "7z" || exit 1

# Function to recursively search in 7zip files
search_7zip_recursive() {
    local file="${1}"
    local temp_dir
    temp_dir=
    if [ -d "/Volumes/RAMDisk/" ]; then
    temp_dir=$(mktemp -d "/Volumes/RAMDisk/eicar_search_XXXXXX")
    elif [ -d "/tmp/RAMDisk/" ]; then
    temp_dir=$(mktemp -d "/tmp/RAMDisk/eicar_search_XXXXXX")
    else
    temp_dir=$(mktemp -d "/tmp/eicar_search_XXXXXX") || return 1
    fi
    
    if ! 7z x "${file}" -o"${temp_dir}" >/dev/null 2>&1; then
        printf "Warning: Failed to extract %s\n" "${file}" >&2
        rm -f -r -v "${temp_dir}"
        return 1
    fi
    
    if rg --fixed-strings --quiet "${input_string}" "${temp_dir}"; then
        printf "%s\n" "${file}"
        rm -f -r -v "${temp_dir}"
        return 0
    fi
    
    # Search in extracted files, including nested archives
    while IFS= read -r -d '' nested_file; do
        if [[ "${nested_file}" == *.7z ]]; then
            if search_7zip_recursive "${nested_file}"; then
                rm -f -r -v "${temp_dir}"
                return 0
            fi
        fi
    done < <(find "${temp_dir}" -type f -print0)
    
    rm -f -r -v "${temp_dir}"
    return 1
}

append_timestamp_to_log "Info: Search in regular files"
# Search in regular files
printf "Info: Searching for EICAR test string in regular files:\n"
regular_files=$(rg --files-with-matches --no-ignore --hidden --fixed-strings -g '!.git/' -g '!*.7z' -g '!*.bz2' "${input_string}" "${DESTINATION}" 2>/dev/null || true)
regular_count=$(printf "%s\n" "${regular_files}" | grep -c '^' || echo 0)

printf "Debug: Regular files found: %d\n" "${regular_count}"
append_timestamp_to_log "Debug: Regular files found: ${regular_count}"
printf "Debug: First few regular files (if any):\n"
append_timestamp_to_log "Debug: First few regular files (if any):"
printf "%s\n" "${regular_files}" | head -n 5
append_timestamp_to_log "${regular_files}"

append_timestamp_to_log "Info: Search in 7z files"

# Search in 7z files
printf "\nInfo: Searching for EICAR test string in 7z files (including nested archives):\n"
zip_files=""
zip_total=0
zip_with_eicar=0
while IFS= read -r -d '' file; do
    zip_total=$((zip_total + 1))
    if search_7zip_recursive "${file}"; then
        zip_files+="${file}"$'\n'
        zip_with_eicar=$((zip_with_eicar + 1))
    fi
    if ((zip_total % 100 == 0)); then
        printf "Info: Processed %d 7z files, found EICAR in %d...\n" "${zip_total}" "${zip_with_eicar}"
    fi
done < <(find "${DESTINATION}" -type f -name "*.7z" -print0)


zip_count="${zip_with_eicar}"
append_timestamp_to_log "Info: zip_count ${zip_count}"

printf "Debug: 7z files containing EICAR: %d\n" "${zip_count}"
append_timestamp_to_log "Debug: 7z files containing EICAR: ${zip_count}"
printf "Debug: First few 7z files with EICAR (if any):\n"
append_timestamp_to_log "Debug: First few 7z files with EICAR (if any): ${zip_count}"
printf "%s\n" "${zip_files}" | head -n 5
append_timestamp_to_log "Info: ${zip_count}"

# Total count
total_count=$((regular_count + zip_count))
append_timestamp_to_log "Info: Total count ${total_count}"

printf "\nInfo: Summary:\n"
append_timestamp_to_log "Info: Summary:"
printf "Info: Number of regular files containing EICAR: %d\n" "${regular_count}"
append_timestamp_to_log "Info: Number of regular files containing EICAR: ${regular_count}"
printf "Info: Number of 7z files containing EICAR: %d (out of %d total 7z files)\n" "${zip_count}" "${zip_total}"
append_timestamp_to_log "Info: Number of 7z files containing EICAR: (out of %d total 7z files) ${zip_count} ${zip_total}"
printf "Info: Total number of files containing EICAR: %d\n" "${total_count}"
append_timestamp_to_log "Info: Total number of files containing EICAR: ${total_count}"

printf "\nInfo: Total number of files in %s:\n" "${DESTINATION}"
append_timestamp_to_log "Info: Total number of files in ${DESTINATION}"
total_files=$(find "${DESTINATION}" -type f | wc -l)
printf "%d\n" "Info: ${total_files}"
append_timestamp_to_log "Info: ${total_files}"

manage_log_copies

printf "\nInfo: Processing complete.\n"
append_timestamp_to_log "Info: Script finished successfully."

cleanup

script_name1="$(basename "${0}")"
printf "\nInfo: script_name1: %s\n" "${script_name1}"
script_path1="$(realpath "$(dirname "${0}")")"
printf "Info: script_path1: %s\n" "${script_path1}"
script_path_with_name="${script_path1}/${script_name1}"
printf "Info: Script path with name: %s\n" "${script_path_with_name}"
printf "Info: Script finished\n"
exit 0
