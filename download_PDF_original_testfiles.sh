#!/usr/bin/env bash
# Author: Pierre Gronau <Pierre.Gronau@ndaal.eu>
# Copyright 2024, 2025
# License: All content is licensed under the terms of the <MIT License>
# Developed on: Debian 12.8x; macOS Sequoia x86 architecture
# Tested on: Debian 12.8x; macOS Sequoia x86 architecture
#
# Note
# This script creates or download files containing the EICAR test string,
# which may trigger antivirus software. Use with caution and only
# in controlled environments for legitimate testing purposes.
#
# Exit on error. Append "|| true" if you expect an error.
set -o errexit
# This is equivalent to set -e. It causes the script to exit
# immediately if any command exits with a non-zero status.
#
# Exit on error inside any functions or subshells.
set -o errtrace
# This setting ensures that the ERR trap is inherited by shell functions,
# command substitutions, and commands executed in a subshell environment.
#
# Do not allow use of undefined vars. Use ${VAR:-} to use an undefined VAR
set -o nounset
# This is equivalent to set -u. It treats unset variables as
# an error when substituting.
#
# Catch the error in case mysqldump fails (but gzip succeeds) in `mysqldump |gzip`
# https://vaneyckt.io/posts/safer_bash
set -o pipefail
# This setting causes a pipeline to return the exit status of the last command
# in the pipe that returned a non-zero status.
#
# Turn on traces, useful while debugging but commented out by default
# set -o xtrace

# Set $IFS to only newline and tab.
#
# http://www.dwheeler.com/essays/filenames-in-shell.html
# nosemgrep: ifs-tampering
IFS=$'\n\t'

# Error trapping function
# Add this line before the error_handler function
# shellcheck disable=SC2317
error_handler() {
    local line_number="${1}"
    local error_code="${2}"
    local last_command="${BASH_COMMAND}"
    printf "Error: Error occurred in line %s (error code: %s)\n" "${line_number}" "${error_code}"
    printf "Error: Failed command: %s\n" "${last_command}"

    # Success case
    return 0
}

# Set the error trap
trap "error_handler ${LINENO} \$?" ERR

cleanup() {
    trap - SIGINT SIGTERM ERR EXIT
    printf "%b\n" "\nInfo: Cleanup is running ..."
    # Additional cleanup tasks can be added here if needed
    rm -f -v ./*.tmp
    printf "%b\n" "\nInfo: Cleanup finished ..."
}

trap cleanup SIGINT SIGTERM EXIT

# Create an array with download URLs
declare -ar download_URLs=(
    "https://pdfa.org/wp-content/uploads/2021/12/NAVAir-native-Creo-TDP.pdf"
    "https://pdfa.org/wp-content/uploads/2021/12/Native-NX-PMI-TDP-Complete.pdf"
    "https://github.com/veraPDF/veraPDF-corpus/blob/staging/PDF_A-4/6.3%20Annotations/6.3.2%20Annotation%20dictionaries/veraPDF%20test%20suite%206-3-2-t01-pass-a.pdf"
)

# Function to download with wget
download_with_wget() {
    local url="${1}"
    if command -v wget &> /dev/null; then
        wget --secure-protocol=auto --https-only --continue --verbose -N --tries=10 --check-certificate "${url}"
    else
        printf "Error: wget is not installed or not in PATH.\n" >&2
        return 1
    fi
}

# Function to download with curl
download_with_curl() {
    local url="${1}"
    if command -v curl &> /dev/null; then
        curl --proto =https --tlsv1.2 -sSf -C - -O --retry 10 --retry-max-time 0 -v "${url}"
    else
        printf "Error: curl is not installed or not in PATH.\n" >&2
        return 1
    fi
}

# Function to download using available method
download_file() {
    local url="${1}"
    if command -v wget &> /dev/null; then
        download_with_wget "${url}"
    elif command -v curl &> /dev/null; then
        download_with_curl "${url}"
    else
        printf "Error: Neither wget nor curl is available.\n" >&2
        return 1
    fi
}

# Main execution
main() {
    for url in "${download_URLs[@]}"; do
        printf "Info: Downloading: %s\n" "${url}"
        if download_file "${url}"; then
            printf "Info: Successfully downloaded: %s\n" "${url}"
        else
            printf "Error: Failed to download: %s\n" "${url}"
        fi
    done
}

main

printf "\nInfo: Processing complete.\n"

cleanup

script_name1="$(basename "${0}")"
printf "\nInfo: script_name1: %s\n" "${script_name1}"
script_path1="$(realpath "$(dirname "${0}")")"
printf "Info: script_path1: %s\n" "${script_path1}"
script_path_with_name="${script_path1}/${script_name1}"
printf "Info: Script path with name: %s\n" "${script_path_with_name}"
printf "Info: Script finished\n"
exit 0
