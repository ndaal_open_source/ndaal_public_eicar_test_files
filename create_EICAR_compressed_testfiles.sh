#!/usr/bin/env bash
# Author: Pierre Gronau <Pierre.Gronau@ndaal.eu>
# Copyright 2024, 2025
# License: All content is licensed under the terms of the <MIT License>
# Developed on: Debian 12.8x; macOS Sequoia x86 architecture
# Tested on: Debian 12.8x; macOS Sequoia x86 architecture
#
# Note
# This script creates or download files containing the EICAR test string,
# which may trigger antivirus software. Use with caution and only
# in controlled environments for legitimate testing purposes.
#
# Exit on error. Append "|| true" if you expect an error.
set -o errexit
# This is equivalent to set -e. It causes the script to exit
# immediately if any command exits with a non-zero status.
#
# Exit on error inside any functions or subshells.
set -o errtrace
# This setting ensures that the ERR trap is inherited by shell functions,
# command substitutions, and commands executed in a subshell environment.
#
# Do not allow use of undefined vars. Use ${VAR:-} to use an undefined VAR
set -o nounset
# This is equivalent to set -u. It treats unset variables as
# an error when substituting.
#
# Catch the error in case mysqldump fails (but gzip succeeds) in `mysqldump |gzip`
# https://vaneyckt.io/posts/safer_bash
set -o pipefail
# This setting causes a pipeline to return the exit status of the last command
# in the pipe that returned a non-zero status.
#
# Turn on traces, useful while debugging but commented out by default
# set -o xtrace

# Set $IFS to only newline and tab.
#
# http://www.dwheeler.com/essays/filenames-in-shell.html
# nosemgrep: ifs-tampering
IFS=$'\n\t'

# Error trapping function
# Add this line before the error_handler function
# shellcheck disable=SC2317
error_handler() {
    local line_number="${1}"
    local error_code="${2}"
    local last_command="${BASH_COMMAND}"
    printf "Error: Error occurred in line %s (error code: %s)\n" "${line_number}" "${error_code}"
    printf "Error: Failed command: %s\n" "${last_command}"

    # Success case
    return 0
}

# Set the error trap
trap "error_handler ${LINENO} \$?" ERR

cleanup() {
    trap - SIGINT SIGTERM ERR EXIT
    printf "%b\n" "\nInfo: Cleanup is running"
    # Additional cleanup tasks can be added here if needed
    rm -f -v ./*.tmp
    rm -f -v "./EICAR_compressed_testfile"*
    printf "%b\n" "\nInfo: Cleanup finished ..."
}

trap cleanup SIGINT SIGTERM EXIT

# Define the EICAR test string
# shellcheck disable=SC2016
input_string='X5O!P%@AP[4\PZX54(P^)7CC)7}$EICAR-STANDARD-ANTIVIRUS-TEST-FILE!$H+H*'
readonly input_string

# Define the EICAR file name
eicar_file="eicar.txt"
readonly eicar_file

# Create a text file with the EICAR test string and ensure it ends with a newline
printf "%s\n" "${input_string}" > "${eicar_file}"

if [ ! -f "${eicar_file}" ]; then
    printf "Error: %s not found. Exiting.\n" "${eicar_file}"
    exit 1
fi

# Define script path and output directory
script_path="${BASH_SOURCE[0]}"
readonly script_path
printf "Info: Current script_path: %s\n" "${script_path}"

script_dir="$(cd "$(dirname "${script_path}")" && pwd)"
readonly script_dir
printf "Info: Current script_dir: %s\n" "${script_dir}"

output_dir="${script_dir}/dataset/EICAR_compressed_nested"
readonly output_dir
printf "Info: Current output_dir: %s\n" "${output_dir}"

# Create output directory if it doesn't exist
if [ ! -d "${output_dir}" ]; then
    if mkdir "${output_dir}"; then
        printf "Info: Created output directory: %s\n" "${output_dir}"
    else
        printf "Error: Failed to create output directory: %s\n" "${output_dir}" >&2
        exit 1
    fi
else
    printf "Info: Output directory already exists: %s\n" "${output_dir}"
fi

printf "Warning: Remove any existing EICAR compressed test file.\n"
rm -f -v "./EICAR_compressed_testfile"*

# Function to check if a command exists
command_exists() {
    command -v "$1" >/dev/null 2>&1
}

# Compression methods
compress_zip() {
    if command_exists zip; then
        zip -j "EICAR_compressed_testfile_zip.zip" "${eicar_file}"
        printf "Info: Created EICAR_compressed_testfile_zip.zip\n"
    else
        printf "Error: zip command not found. Skipping zip compression.\n"
    fi
}

compress_7zip() {
    if command_exists 7z; then
        7z a "EICAR_compressed_testfile_7zip.7z" "${eicar_file}"
        printf "Info: Created EICAR_compressed_testfile_7zip.7z\n"
    else
        printf "Error: 7z command not found. Skipping 7zip compression.\n"
    fi
}

compress_lha() {
    if command_exists lha; then
        lha a "EICAR_compressed_testfile_lha.lzh" "${eicar_file}" || true
        printf "Info: Created EICAR_compressed_testfile_lha.lzh\n"
    else
        printf "Error: lha command not found. Skipping lha compression.\n"
    fi
}

compress_gzip() {
    if command_exists gzip; then
        gzip -c "${eicar_file}" > "EICAR_compressed_testfile_gzip.gz"
        printf "Info: Created EICAR_compressed_testfile_gzip.gz\n"
    else
        printf "Error: gzip command not found. Skipping gzip compression.\n"
    fi
}

compress_bzip2() {
    if command_exists bzip2; then
        bzip2 -c "${eicar_file}" > "EICAR_compressed_testfile_bzip2.bz2"
        printf "Info: Created EICAR_compressed_testfile_bzip2.bz2\n"
    else
        printf "Error: bzip2 command not found. Skipping bzip2 compression.\n"
    fi
}

compress_xz() {
    if command_exists xz; then
        xz -c "${eicar_file}" > "EICAR_compressed_testfile_xz.xz"
        printf "Info: Created EICAR_compressed_testfile_xz.xz\n"
    else
        printf "Error: xz command not found. Skipping xz compression.\n"
    fi
}

compress_tar() {
    if command_exists tar; then
        tar -cvf "EICAR_compressed_testfile_tar.tar" "${eicar_file}"
        printf "Info: Created EICAR_compressed_testfile_tar.tar\n"
    else
        printf "Error: tar command not found. Skipping tar compression.\n"
    fi
}

compress_rar() {
    if command_exists rar; then
        rar a "EICAR_compressed_testfile_rar.rar" "${eicar_file}"
        printf "Info: Created EICAR_compressed_testfile_rar.rar\n"
    else
        printf "Error: rar command not found. Skipping rar compression.\n"
    fi
}

compress_zoo() {
    if command_exists zoo; then
        zoo a "EICAR_compressed_testfile_zoo.zoo" "${eicar_file}"
        printf "Info: Created EICAR_compressed_testfile_zoo.zoo\n"
    else
        printf "Error: zoo command not found. Skipping zoo compression.\n"
    fi
}

compress_arc() {
    if command_exists arc; then
        if arc a "EICAR_compressed_testfile_arc.arc" "${eicar_file}" 2>&1 | tee /dev/stderr | grep -q "is not an archive"; then
            printf "Warning: arc compression failed. This may be due to the special nature of the EICAR file.\n"
            printf "Arc output: "
            arc a s "EICAR_compressed_testfile_arc.arc" "${eicar_file}"
        else
            printf "Creating EICAR_compressed_testfile_arc.arc failed\n"
            ls -la EICAR_compressed_testfile_arc.arc || true
        fi
    else
        printf "Error: arc command not found. Skipping arc compression.\n"
    fi
}

compress_zstd() {
    if command_exists zstd; then
        zstd -c "${eicar_file}" > "EICAR_compressed_testfile_zstd.zst"
        printf "Info: Created EICAR_compressed_testfile_zstd.zst\n"
    else
        printf "Error: zstd command not found. Skipping zstd compression.\n"
    fi
}

compress_lz4() {
    if command_exists lz4; then
        lz4 -c "${eicar_file}" > "EICAR_compressed_testfile_lz4.lz4"
        printf "Info: Created EICAR_compressed_testfile_lz4.lz4\n"
    else
        printf "Error: lz4 command not found. Skipping lz4 compression.\n"
    fi
}

compress_lzma() {
    if command_exists lzma; then
        lzma -c "${eicar_file}" > "EICAR_compressed_testfile_lzma.lzma"
        printf "Info: Created EICAR_compressed_testfile_lzma.lzma\n"
    else
        printf "Error: lzma command not found. Skipping lzma compression.\n"
    fi
}

compress_lzip() {
    if command_exists lzip; then
        lzip -c "${eicar_file}" > "EICAR_compressed_testfile_lzip.lz"
        printf "Info: Created EICAR_compressed_testfile_lzip.lz\n"
    else
        printf "Error: lzip command not found. Skipping lzip compression.\n"
    fi
}

compress_lrzip() {
    if command_exists lrzip; then
        lrzip -o "EICAR_compressed_testfile_lrzip.lrz" "${eicar_file}"
        printf "Info: Created EICAR_compressed_testfile_lrzip.lrz\n"
    else
        printf "Error: lrzip command not found. Skipping lrzip compression.\n"
    fi
}

compress_rzip() {
    if command_exists rzip; then
        rzip -k "${eicar_file}"
        mv "${eicar_file}.rz" "EICAR_compressed_testfile_rzip.rz"
        printf "Info: Created EICAR_compressed_testfile_rzip.rz\n"
    else
        printf "Error: rzip command not found. Skipping rzip compression.\n"
    fi
}

compress_arj() {
    if command_exists arj; then
        arj a "EICAR_compressed_testfile_arj.arj" "${eicar_file}"
        printf "Info: Created EICAR_compressed_testfile_arj.arj\n"
    else
        printf "Error: arj command not found. Skipping arj compression.\n"
    fi
}

compress_lzop() {
    if command_exists lzop; then
        lzop -c "${eicar_file}" > "EICAR_compressed_testfile_lzop.lzo"
        printf "Info: Created EICAR_compressed_testfile_lzop.lzo\n"
    else
        printf "Error: lzop command not found. Skipping lzop compression.\n"
    fi
}

compress_zpaq() {
    if command_exists zpaq; then
        zpaq add "EICAR_compressed_testfile_zpaq.zpaq" "${eicar_file}"
        printf "Info: Created EICAR_compressed_testfile_zpaq.zpaq\n"
    else
        printf "Error: zpaq command not found. Skipping zpaq compression.\n"
    fi
}

compress_squashfs() {
    if command_exists mksquashfs; then
        mksquashfs "${eicar_file}" "EICAR_compressed_testfile_squashfs.squashfs"
        printf "Info: Created EICAR_compressed_testfile_squashfs.squashfs\n"
    else
        printf "Error: mksquashfs command not found. Skipping squashfs compression.\n"
    fi
}

compress_cpio() {
    if command_exists cpio; then
        printf "%s" "${eicar_file}" | cpio -ov > "EICAR_compressed_testfile_cpio.cpio"
        printf "Info: Created EICAR_compressed_testfile_cpio.cpio\n"
    else
        printf "Error: cpio command not found. Skipping cpio compression.\n"
    fi
}

compress_brotli() {
    if command_exists brotli; then
        brotli -c "${eicar_file}" > "EICAR_compressed_testfile_brotli.br"
        printf "Info: Created EICAR_compressed_testfile_brotli.br\n"
    else
        printf "Error: brotli command not found. Skipping brotli compression.\n"
    fi
}

compress_zopfli() {
    if command_exists zopfli; then
        zopfli "${eicar_file}"
        mv "${eicar_file}.gz" "EICAR_compressed_testfile_zopfli.gz"
        printf "Info: Created EICAR_compressed_testfile_zopfli.gz\n"
    else
        printf "Error: zopfli command not found. Skipping zopfli compression.\n"
    fi
}

compress_lzham() {
    if command_exists lzham; then
        lzham compress "${eicar_file}" "EICAR_compressed_testfile_lzham.lzham"
        printf "Info: Created EICAR_compressed_testfile_lzham.lzham\n"
    else
        printf "Error: lzham command not found. Skipping lzham compression.\n"
    fi
}

compress_7zip_lzham() {
    if command_exists 7z; then
        if 7z -m0=LZHAM -mx=8 a "EICAR_compressed_testfile_7zip_lzham.7z" "${eicar_file}" 2>&1 | grep -q "ERROR"; then
            printf "7-Zip LZHAM compression failed. Falling back to standard LZMA.\n"
            7z a "EICAR_compressed_testfile_7zip_lzma.7z" "${eicar_file}"
            printf "Info: Created EICAR_compressed_testfile_7zip_lzma.7z (using standard LZMA)\n"
        else
            printf "Info: Created EICAR_compressed_testfile_7zip_lzham.7z\n"
        fi
    else
        printf "Error: 7z command not found. Skipping 7-Zip LZHAM/LZMA compression.\n"
    fi
}

compress_lzfse() {
    if command_exists lzfse; then
        lzfse -encode -i "${eicar_file}" -o "EICAR_compressed_testfile_lzfse.lzfse"
        printf "Info: Created EICAR_compressed_testfile_lzfse.lzfse\n"
    else
        printf "Error: lzfse command not found. Skipping lzfse compression.\n"
    fi
}

compress_lzjb() {
    if command_exists lzjb; then
        lzjb < "${eicar_file}" > "EICAR_compressed_testfile_lzjb.lzjb"
        printf "Info: Created EICAR_compressed_testfile_lzjb.lzjb\n"
    else
        printf "Error: lzjb command not found. Skipping lzjb compression.\n"
    fi
}

compress_lzrw() {
    if command_exists lzrw; then
        lzrw -e "${eicar_file}" "EICAR_compressed_testfile_lzrw.lzrw"
        printf "Info: Created EICAR_compressed_testfile_lzrw.lzrw\n"
    else
        printf "Error: lzrw command not found. Skipping lzrw compression.\n"
    fi
}

compress_lzss() {
    if command_exists lzss; then
        lzss e "${eicar_file}" "EICAR_compressed_testfile_lzss.lzss"
        printf "Info: Created EICAR_compressed_testfile_lzss.lzss\n"
    else
        printf "Error: lzss command not found. Skipping lzss compression.\n"
    fi
}

compress_deflate64() {
    if command_exists zip; then
        if zip -v | grep -q "Zip 3.0" && zip -v | grep -q "deflate64"; then
            zip -Z deflate64 "EICAR_compressed_testfile_deflate64.zip" "${eicar_file}"
            printf "Info: Created EICAR_compressed_testfile_deflate64.zip\n"
        else
            printf "zip doesn't support deflate64. Using standard deflate instead.\n"
            zip -9 "EICAR_compressed_testfile_deflate_max.zip" "${eicar_file}"
            if unzip -l "EICAR_compressed_testfile_deflate_max.zip" | grep -q "stored"; then
                printf "Info: Created EICAR_compressed_testfile_deflate_max.zip (file stored without compression due to small size)\n"
            else
                printf "Info: Created EICAR_compressed_testfile_deflate_max.zip (using standard deflate)\n"
            fi
        fi
    else
        printf "Error: zip command not found. Skipping deflate64/deflate compression.\n"
    fi
}

compress_pigz() {
    if command_exists pigz; then
        pigz -c "${eicar_file}" > "EICAR_compressed_testfile_pigz.gz"
        printf "Info: Created EICAR_compressed_testfile_pigz.gz\n"
    else
        printf "Error: pigz command not found. Skipping pigz compression.\n"
    fi
}

compress_pixz() {
    if command_exists pixz; then
        pixz < "${eicar_file}" > "EICAR_compressed_testfile_pixz.xz"
        printf "Info: Created EICAR_compressed_testfile_pixz.xz\n"
    else
        printf "Error: pixz command not found. Skipping pixz compression.\n"
    fi
}

compress_plzip() {
    if command_exists plzip; then
        plzip -c "${eicar_file}" > "EICAR_compressed_testfile_plzip.lz"
        printf "Info: Created EICAR_compressed_testfile_plzip.lz\n"
    else
        printf "Error: plzip command not found. Skipping plzip compression.\n"
    fi
}

# Perform compressions
compress_zip
compress_7zip
compress_lha
compress_gzip
compress_bzip2
compress_xz
compress_tar
compress_rar
compress_zoo
compress_arc
compress_zstd
compress_lz4
compress_lzma
compress_lzip
compress_lrzip
compress_rzip
compress_arj
compress_lzop
compress_zpaq
compress_squashfs
compress_cpio
compress_brotli
#compress_lzham
compress_7zip_lzham
compress_lzfse
compress_lzjb
compress_lzrw
compress_lzss
compress_deflate64
compress_pigz
compress_pixz
compress_plzip

compress_zopfli

ls -la "./EICAR_compressed_testfile"*

mv -v "./EICAR_compressed_testfile"* "${output_dir}"

printf "Info: Compression operations completed.\n"

cleanup

script_name1="$(basename "${0}")"
printf "\nInfo: script_name1: %s\n" "${script_name1}"
script_path1="$(realpath "$(dirname "${0}")")"
printf "Info: script_path1: %s\n" "${script_path1}"
script_path_with_name="${script_path1}/${script_name1}"
printf "Info: Script path with name: %s\n" "${script_path_with_name}"
printf "Info: Script finished\n"
exit 0
