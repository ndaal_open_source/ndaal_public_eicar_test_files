#!/usr/bin/env bash
# Author: Pierre Gronau <Pierre.Gronau@ndaal.eu>
# Copyright 2024, 2025
# License: All content is licensed under the terms of the <MIT License>
# Developed on: Debian 12.9x; macOS Sequoia x86 architecture
# Tested on: Debian 12.9x; macOS Sequoia x86 architecture
#
# Note
# This script creates or download files containing the EICAR test string,
# which may trigger antivirus software. Use with caution and only
# in controlled environments for legitimate testing purposes.
#
# Exit on error. Append "|| true" if you expect an error.
set -o errexit
# This is equivalent to set -e. It causes the script to exit
# immediately if any command exits with a non-zero status.
#
# Exit on error inside any functions or subshells.
set -o errtrace
# This setting ensures that the ERR trap is inherited by shell functions,
# command substitutions, and commands executed in a subshell environment.
#
# Do not allow use of undefined vars. Use ${VAR:-} to use an undefined VAR
set -o nounset
# This is equivalent to set -u. It treats unset variables as
# an error when substituting.
#
# Catch the error in case mysqldump fails (but gzip succeeds) in `mysqldump |gzip`
# https://vaneyckt.io/posts/safer_bash
set -o pipefail
# This setting causes a pipeline to return the exit status of the last command
# in the pipe that returned a non-zero status.
#
# Turn on traces, useful while debugging but commented out by default
# set -o xtrace

# Set $IFS to only newline and tab.
#
# http://www.dwheeler.com/essays/filenames-in-shell.html
# nosemgrep: ifs-tampering
IFS=$'\n\t'

# Error trapping function
# Add this line before the error_handler function
# shellcheck disable=SC2317
error_handler() {
    local line_number="${1}"
    local error_code="${2}"
    local last_command="${BASH_COMMAND}"
    printf "Error: Error occurred in line %s (error code: %s)\n" "${line_number}" "${error_code}"
    printf "Error: Failed command: %s\n" "${last_command}"

    # Success case
    return 0
}

# Set the error trap
trap "error_handler ${LINENO} \$?" ERR

cleanup() {
    trap - SIGINT SIGTERM ERR EXIT
    printf "%b\n" "\nInfo: Cleanup is running ..."
    # Additional cleanup tasks can be added here if needed
    rm -f -v ./*.tmp
    printf "%b\n" "\nInfo: Cleanup finished ..."
}

trap cleanup SIGINT SIGTERM EXIT
# Define the EICAR test string
# shellcheck disable=SC2016
input_string='X5O!P%@AP[4\PZX54(P^)7CC)7}$EICAR-STANDARD-ANTIVIRUS-TEST-FILE!$H+H*'
readonly input_string

# Define the EICAR file name
eicar_file="eicar.txt"
readonly eicar_file

# Create a text file with the EICAR test string and ensure it ends with a newline
printf "%s\n" "${input_string}" > "${eicar_file}"

if [ ! -f "${eicar_file}" ]; then
    printf "Error: %s not found. Exiting.\n" "${eicar_file}"
    exit 1
fi

printf "Remove any existing EICAR test files.\n"
rm -f -v "./EICAR_testfile_"*

# Declare and assign separately to avoid masking return values
SCRIPT_PATH=$(dirname "${0}")
SCRIPT_PATH=$(realpath "${SCRIPT_PATH}")
readonly SCRIPT_PATH
printf "Info: Current SCRIPT_PATH: %s\n" "${SCRIPT_PATH}"

# Define and create directories
DESTINATION="${SCRIPT_PATH}/dataset/EICAR_128_characters"
readonly DESTINATION
printf "Info: Destination Directory: %s\n" "${DESTINATION}"

declare -ar directories=(
    "${DESTINATION}"
    "${DESTINATION}/EICAR_testfile_CR"
    "${DESTINATION}/EICAR_testfile_CTRL-Z"
    "${DESTINATION}/EICAR_testfile_LF"
    "${DESTINATION}/EICAR_testfile_permutations"
    "${DESTINATION}/EICAR_testfile_space"
    "${DESTINATION}/EICAR_testfile_tab"
)

# Remove existing EICAR test files
printf "Warning: Removing any existing EICAR test files.\n"
if [ -d "${DESTINATION}" ]; then
    find "${DESTINATION}" -type f -name "EICAR_testfile_*" -delete -print
fi

# Create directories and remove existing files
for DIRECTORY in "${directories[@]}"; do
    printf "%s\n" "${DIRECTORY}"
    if [ ! -d "${DIRECTORY}" ]; then
        mkdir -p -v "${DIRECTORY}"
        printf "Info: the directory %s is created.\n" "${DIRECTORY}"
    fi
done

# Function to create permutations
create_permutations() {
    local char="${1}"
    local char_name="${2}"
    local max_count=129

    for ((i=1; i<=max_count; i++)); do
        local filename="EICAR_testfile_${char_name}_${i}_permutation.txt"
        printf "%s%*s" "${input_string}" "${i}" "${char}" | tr ' ' "${char}" > "${filename}"
        printf "Created %s\n" "${filename}"
    done
}

# Create space permutations
create_permutations " " "space"

# Create tab permutations
create_permutations $'\t' "tab"

# Create LF permutations
create_permutations $'\n' "LF"

# Create CR permutations
create_permutations $'\r' "CR"

# Create CTRL-Z permutations
create_permutations $'\032' "CTRL-Z"

# Function to create combinations of all variations
create_combinations() {
    local max_additional_chars=128
    local eicar_length="${#input_string}"
    local chars=(" " $'\t' $'\n' $'\r' $'\032')
    local total_chars="${#chars[@]}"

    for ((i=0; i<=max_additional_chars; i++)); do
        local filename="EICAR_testfile_${i}_additional_characters_permutation.txt"
        printf "%s" "${input_string}" > "${filename}"
        
        for ((j=0; j<i; j++)); do
            local random_char="${chars[$((RANDOM % total_chars))]}"
            printf "%s" "${random_char}" >> "${filename}"
        done
        
        printf "Created %s (total length: %d)\n" "${filename}" $((eicar_length + i))
    done
}

# Create combinations of all variations
create_combinations

# List all created files with detailed information
printf '\nInfo: Detailed file information:\n'
ls -l "${eicar_file}" "./EICAR_testfile_"*

printf '\nInfo: Moving EICAR_testfiles to specific directories.\n'
mv -v "./EICAR_testfile_CR_"* "${DESTINATION}/EICAR_testfile_CR"
mv -v "./EICAR_testfile_CTRL-Z_"* "${DESTINATION}/EICAR_testfile_CTRL-Z"
mv -v "./EICAR_testfile_LF_"* "${DESTINATION}/EICAR_testfile_LF"
mv -v "./EICAR_testfile_space_"* "${DESTINATION}/EICAR_testfile_space"
mv -v "./EICAR_testfile_tab_"* "${DESTINATION}/EICAR_testfile_tab"
mv -v "./EICAR_testfile_"* "${DESTINATION}/EICAR_testfile_permutations"

printf "Info: All permutations and combinations have been created.\n"

cleanup

script_name1="$(basename "${0}")"
printf "\nInfo: script_name1: %s\n" "${script_name1}"
script_path1="$(realpath "$(dirname "${0}")")"
printf "Info: script_path1: %s\n" "${script_path1}"
script_path_with_name="${script_path1}/${script_name1}"
printf "Info: Script path with name: %s\n" "${script_path_with_name}"
printf "Info: Script finished\n"
exit 0
