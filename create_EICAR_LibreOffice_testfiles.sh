#!/usr/bin/env bash
# Author: Pierre Gronau <Pierre.Gronau@ndaal.eu>
# Copyright 2024, 2025
# License: All content is licensed under the terms of the <Apache 2.0>
# Developed on: Debian 12.9x; macOS Sequoia x86 architecture
# Tested on: Debian 12.9x; macOS Sequoia x86 architecture
#
# Note
# This script creates or download files containing the EICAR test string,
# which may trigger antivirus software. Use with caution and only
# in controlled environments for legitimate testing purposes.
#
# Exit on error. Append "|| true" if you expect an error.
set -o errexit
# This is equivalent to set -e. It causes the script to exit
# immediately if any command exits with a non-zero status.
#
# Exit on error inside any functions or subshells.
set -o errtrace
# This setting ensures that the ERR trap is inherited by shell functions,
# command substitutions, and commands executed in a subshell environment.
#
# Do not allow use of undefined vars. Use ${VAR:-} to use an undefined VAR
set -o nounset
# This is equivalent to set -u. It treats unset variables as
# an error when substituting.
#
# Catch the error in case mysqldump fails (but gzip succeeds) in `mysqldump |gzip`
# https://vaneyckt.io/posts/safer_bash
set -o pipefail
# This setting causes a pipeline to return the exit status of the last command
# in the pipe that returned a non-zero status.
#
# Turn on traces, useful while debugging but commented out by default
# set -o xtrace

# Set $IFS to only newline and tab.
#
# http://www.dwheeler.com/essays/filenames-in-shell.html
# nosemgrep: ifs-tampering
IFS=$'\n\t'

# Error trapping function
# Add this line before the error_handler function
# shellcheck disable=SC2317
error_handler() {
    local line_number="${1}"
    local error_code="${2}"
    local last_command="${BASH_COMMAND}"
    printf "Error: Error occurred in line %s (error code: %s)\n" "${line_number}" "${error_code}"
    printf "Error: Failed command: %s\n" "${last_command}"

    # Success case
    return 0
}

# Set the error trap
trap "error_handler ${LINENO} \$?" ERR

cleanup() {
    trap - SIGINT SIGTERM ERR EXIT
    printf "%b\n" "\nInfo: Cleanup is running ..."
    # Additional cleanup tasks can be added here if needed
    rm -f -v ./*.tmp
    rm -f -v "./EICAR_LibreOffice_"*
    printf "%b\n" "\nInfo: Cleanup finished ...
}

trap cleanup SIGINT SIGTERM ERR EXIT

# Define the EICAR test string
# shellcheck disable=SC2016
input_string='X5O!P%@AP[4\PZX54(P^)7CC)7}$EICAR-STANDARD-ANTIVIRUS-TEST-FILE!$H+H*'
readonly input_string

# Define the EICAR file name
eicar_file="eicar.txt"
readonly eicar_file

# Create a text file with the EICAR test string and ensure it ends with a newline
printf "%s\n" "${input_string}" > "${eicar_file}"

if [ ! -f "${eicar_file}" ]; then
    printf "Error: %s not found. Exiting.\n" "${eicar_file}"
    exit 1
fi

# Define script path and output directory
script_path="${BASH_SOURCE[0]}"
readonly script_path
printf "Info: Current script_path: %s\n" "${script_path}"

script_dir="$(cd "$(dirname "${script_path}")" && pwd)"
readonly script_dir
printf "Info: Current script_dir: %s\n" "${script_dir}"

output_dir="${script_dir}/dataset/EICAR_compressed_nested"
readonly output_dir
printf "Info: Current output_dir: %s\n" "${output_dir}"

# Create output directory if it doesn't exist
if [ ! -d "${output_dir}" ]; then
    if mkdir "${output_dir}"; then
        printf "Info: Created output directory: %s\n" "${output_dir}"
    else
        printf "Error: Failed to create output directory: %s\n" "${output_dir}" >&2
        exit 1
    fi
else
    printf "Output directory already exists: %s\n" "${output_dir}"
fi

# Function to check if LibreOffice is installed and set the command
check_libreoffice() {
    if command -v libreoffice &> /dev/null; then
        LIBREOFFICE_CMD="libreoffice"
    elif [ -f "/Applications/LibreOffice.app/Contents/MacOS/soffice" ]; then
        LIBREOFFICE_CMD="/Applications/LibreOffice.app/Contents/MacOS/soffice"
    else
        printf "Error: LibreOffice is not installed or not found in the expected location.\n" >&2
        exit 1
    fi
    printf "Using LibreOffice command: %s\n" "${LIBREOFFICE_CMD}"
}

# Function to create ODT file with EICAR string
create_odt_file() {
    local output_file="${output_dir}/eicar.odt"
    local temp_file
    temp_file="$(mktemp)"
    
    printf "%s" "${input_string}" > "${temp_file}"
    
    if "${LIBREOFFICE_CMD}" --headless --convert-to odt "${temp_file}" --outdir "${output_dir}" 2>&1; then
        local new_file
        new_file=$(find "${output_dir}" -type f -name "*.odt" -newer "${temp_file}" -print -quit)
        
        if [ -n "${new_file}" ]; then
            mv "${new_file}" "${output_file}"
            printf "Created ODT file: %s\n" "${output_file}"
        else
            printf "Error: Failed to find created ODT file\n" >&2
        fi
    else
        printf "Error: Failed to create ODT file. LibreOffice conversion failed.\n" >&2
    fi
    
    rm -f "${temp_file}"
}

# Function to attempt conversion to other formats
convert_to_format() {
    local input_file="${output_dir}/eicar.odt"
    local output_format="$1"
    local output_file="${output_dir}/EICAR_LibreOffice_${output_format}.${output_format}"
    
    if "${LIBREOFFICE_CMD}" --headless --convert-to "${output_format}" "${input_file}" --outdir "${output_dir}" 2>&1; then
        local new_file
        new_file=$(find "${output_dir}" -type f -name "*.${output_format}" -newer "${input_file}" -print -quit)
        
        if [ -n "${new_file}" ]; then
            mv "${new_file}" "${output_file}"
            printf "Created %s file: %s\n" "${output_format}" "${output_file}"
        else
            printf "Error: Failed to find created %s file\n" "${output_format}" >&2
        fi
    else
        printf "Error: Failed to create %s file. LibreOffice conversion failed.\n" "${output_format}" >&2
    fi
}

# Function to list available filters
list_filters() {
    printf "Available LibreOffice filters:\n"
    "${LIBREOFFICE_CMD}" --headless --convert-to pdf --show
}

# Main execution
main() {
    check_libreoffice
    create_odt_file
    convert_to_format "ods"
    convert_to_format "odp"
    convert_to_format "odg"
    printf "Attempted to create all EICAR test files in: %s\n" "${output_dir}"
    list_filters
}

main

cleanup

script_name1="$(basename "${0}")"
printf "\nInfo: script_name1: %s\n" "${script_name1}"
script_path1="$(realpath "$(dirname "${0}")")"
printf "Info: script_path1: %s\n" "${script_path1}"
script_path_with_name="${script_path1}/${script_name1}"
printf "Info: Script path with name: %s\n" "${script_path_with_name}"
printf "Info: Script finished\n"
exit 0
