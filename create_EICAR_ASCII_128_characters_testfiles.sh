#!/usr/bin/env bash
# Author: Pierre Gronau <Pierre.Gronau@ndaal.eu>
# Copyright 2024, 2025
# License: All content is licensed under the terms of the <MIT License>
# Developed on: Debian 12.x; macOS Sonoma x86 architecture
# Tested on: Debian 12.x; macOS Sonoma x86 architecture
#
# Note
# This script creates or download files containing the EICAR test string, 
# which may trigger antivirus software. Use with caution and only 
# in controlled environments for legitimate testing purposes.
#
# Exit on error. Append "|| true" if you expect an error.
set -o errexit
# This is equivalent to set -e. It causes the script to exit 
# immediately if any command exits with a non-zero status.
#
# Exit on error inside any functions or subshells.
set -o errtrace
# This setting ensures that the ERR trap is inherited by shell functions,
# command substitutions, and commands executed in a subshell environment.
#
# Do not allow use of undefined vars. Use ${VAR:-} to use an undefined VAR
set -o nounset
# This is equivalent to set -u. It treats unset variables as 
# an error when substituting.
#
# Catch the error in case mysqldump fails (but gzip succeeds) in `mysqldump |gzip`
# https://vaneyckt.io/posts/safer_bash
#set -o pipefail
# This setting causes a pipeline to return the exit status of the last command
# in the pipe that returned a non-zero status.
#
# Turn on traces, useful while debugging but commented out by default
# set -o xtrace

# Set $IFS to only newline and tab.
#
# http://www.dwheeler.com/essays/filenames-in-shell.html
# nosemgrep: ifs-tampering
IFS=$'\n\t'

# Error trapping function
# Add this line before the error_handler function
# shellcheck disable=SC2317
error_handler() {
    local line_number="${1}"
    local error_code="${2}"
    local last_command="${BASH_COMMAND}"
    printf "Error: Error occurred in line %s (error code: %s)\n" "${line_number}" "${error_code}"
    printf "Error: Failed command: %s\n" "${last_command}"

    # Success case
    return 0
}

# Set the error trap
trap "error_handler ${LINENO} \$?" ERR

cleanup() {
    trap - SIGINT SIGTERM ERR EXIT
    printf "%b\n" "\nInfo: Cleanup is running ..."
    # Additional cleanup tasks can be added here if needed
    rm -f -v ./*.tmp
    printf "%b\n" "\nInfo: Cleanup finished ..."
}

trap cleanup SIGINT SIGTERM EXIT

# Define the EICAR test string
# shellcheck disable=SC2016
input_string='X5O!P%@AP[4\PZX54(P^)7CC)7}$EICAR-STANDARD-ANTIVIRUS-TEST-FILE!$H+H*'
readonly input_string

eicar_file="eicar.txt"
readonly eicar_file

printf "%s\n" "${input_string}" > "${eicar_file}"

if [ ! -f "${eicar_file}" ]; then
    printf "Error: %s not found. Exiting.\n" "${eicar_file}"
    exit 1
fi

printf "Warning: Remove any existing EICAR test files.\n"
rm -f -v "./EICAR_ASCII_testfile_"*

# Declare and assign separately to avoid masking return values
SCRIPT_PATH=$(dirname "${0}")
SCRIPT_PATH=$(realpath "${SCRIPT_PATH}")
readonly SCRIPT_PATH

DESTINATION="${SCRIPT_PATH}/dataset/EICAR_128_characters/EICAR_ASCII"
readonly DESTINATION
printf "Info: Destination Directory: %s\n" "${DESTINATION}"

# Remove existing EICAR test files
printf "Warning: Removing any existing EICAR test files.\n"
if [ -d "${DESTINATION}" ]; then
    find "${DESTINATION}" -type f -name "EICAR_ASCII_testfile_*" -delete -print
    rm -rf "${DESTINATION}/EICAR_ASCII_testfile_"*
fi

mkdir -p "${DESTINATION}"

create_permutations() {
    local dec="${1}"
    local hex="${2}"
    local oct="${3}"
    local ascii="${4}"
    local safe_ascii="${5}"
    local max_count=129
    local dir_name="${DESTINATION}/EICAR_ASCII_testfile_${safe_ascii}"

    mkdir -p "${dir_name}"
    printf "Info: Creating permutations for ASCII character: %s\n" "${ascii}"

    for ((i=1; i<=max_count; i++)); do
        local filename="${dir_name}/EICAR_ASCII_testfile_${dec}_${hex}_${oct}_${safe_ascii}_${i}_permutation.txt"
        printf '%s' "${input_string}" > "${filename}"
        for ((j=0; j<i; j++)); do
            printf '%b' "\\$(printf '%03o' "${dec#0}")" >> "${filename}"
        done
        printf "Info: Created %s\n" "${filename}"
    done
}

# Generate permutations for all ASCII characters (0-127)
for i in {0..127}; do
    dec=$(printf "%03d" "${i}")
    hex=$(printf "%02X" "${i}")
    oct=$(printf "%03o" "${i}")
    
    case "${i}" in
        0) ascii="NUL"; safe_ascii="NUL" ;;
        1) ascii="SOH"; safe_ascii="SOH" ;;
        2) ascii="STX"; safe_ascii="STX" ;;
        3) ascii="ETX"; safe_ascii="ETX" ;;
        4) ascii="EOT"; safe_ascii="EOT" ;;
        5) ascii="ENQ"; safe_ascii="ENQ" ;;
        6) ascii="ACK"; safe_ascii="ACK" ;;
        7) ascii="BEL"; safe_ascii="BEL" ;;
        8) ascii="BS"; safe_ascii="BS" ;;
        9) ascii="HT"; safe_ascii="HT" ;;
        10) ascii="LF"; safe_ascii="LF" ;;
        11) ascii="VT"; safe_ascii="VT" ;;
        12) ascii="FF"; safe_ascii="FF" ;;
        13) ascii="CR"; safe_ascii="CR" ;;
        14) ascii="SO"; safe_ascii="SO" ;;
        15) ascii="SI"; safe_ascii="SI" ;;
        16) ascii="DLE"; safe_ascii="DLE" ;;
        17) ascii="DC1"; safe_ascii="DC1" ;;
        18) ascii="DC2"; safe_ascii="DC2" ;;
        19) ascii="DC3"; safe_ascii="DC3" ;;
        20) ascii="DC4"; safe_ascii="DC4" ;;
        21) ascii="NAK"; safe_ascii="NAK" ;;
        22) ascii="SYN"; safe_ascii="SYN" ;;
        23) ascii="ETB"; safe_ascii="ETB" ;;
        24) ascii="CAN"; safe_ascii="CAN" ;;
        25) ascii="EM"; safe_ascii="EM" ;;
        26) ascii="SUB"; safe_ascii="SUB" ;;
        27) ascii="ESC"; safe_ascii="ESC" ;;
        28) ascii="FS"; safe_ascii="FS" ;;
        29) ascii="GS"; safe_ascii="GS" ;;
        30) ascii="RS"; safe_ascii="RS" ;;
        31) ascii="US"; safe_ascii="US" ;;
        32) ascii="SP"; safe_ascii="SP" ;;
        33) ascii="!"; safe_ascii="EXCLAMATION" ;;
        34) ascii='"'; safe_ascii="QUOTATION" ;;
        35) ascii="#"; safe_ascii="HASH" ;;
        36) ascii="$"; safe_ascii="DOLLAR" ;;
        37) ascii="%"; safe_ascii="PERCENT" ;;
        38) ascii="&"; safe_ascii="AMPERSAND" ;;
        39) ascii="'"; safe_ascii="APOSTROPHE" ;;
        40) ascii="("; safe_ascii="LEFTPARENTHESIS" ;;
        41) ascii=")"; safe_ascii="RIGHTPARENTHESIS" ;;
        42) ascii="*"; safe_ascii="ASTERISK" ;;
        43) ascii="+"; safe_ascii="PLUS" ;;
        44) ascii=","; safe_ascii="COMMA" ;;
        45) ascii="-"; safe_ascii="HYPHEN" ;;
        46) ascii="."; safe_ascii="PERIOD" ;;
        47) ascii="/"; safe_ascii="SLASH" ;;
        58) ascii=":"; safe_ascii="COLON" ;;
        59) ascii=";"; safe_ascii="SEMICOLON" ;;
        60) ascii="<"; safe_ascii="LESSTHAN" ;;
        61) ascii="="; safe_ascii="EQUALS" ;;
        62) ascii=">"; safe_ascii="GREATERTHAN" ;;
        63) ascii="?"; safe_ascii="QUESTION" ;;
        64) ascii="@"; safe_ascii="AT" ;;
        91) ascii="["; safe_ascii="LEFTSQUAREBRACKET" ;;
        92) ascii="\\"; safe_ascii="BACKSLASH" ;;
        93) ascii="]"; safe_ascii="RIGHTSQUAREBRACKET" ;;
        94) ascii="^"; safe_ascii="CARET" ;;
        95) ascii="_"; safe_ascii="UNDERSCORE" ;;
        96) ascii="\`"; safe_ascii="BACKTICK" ;;
        123) ascii="{"; safe_ascii="LEFTCURLYBRACE" ;;
        124) ascii="|"; safe_ascii="VERTICALBAR" ;;
        125) ascii="}"; safe_ascii="RIGHTCURLYBRACE" ;;
        126) ascii="~"; safe_ascii="TILDE" ;;
        127) ascii="DEL"; safe_ascii="DEL" ;;
        *)
            ascii=$(printf '%b' "\\$(printf '%03o' "${i}")")
            if [[ "${i}" -ge 65 && "${i}" -le 90 ]]; then
                safe_ascii="UPPERCASE_${ascii}"
            elif [[ "${i}" -ge 97 && "${i}" -le 122 ]]; then
                safe_ascii="LOWERCASE_${ascii}"
            elif [[ "${i}" -ge 48 && "${i}" -le 57 ]]; then
                safe_ascii="DIGIT_${ascii}"
            else
                safe_ascii="CHR${dec}"
            fi
            ;;
    esac

    create_permutations "${dec}" "${hex}" "${oct}" "${ascii}" "${safe_ascii}"
done

printf "%s\n" "Info: All permutations have been created."

printf "%s\n" "Info: Listing created directories:"
ls -d "${DESTINATION}"/EICAR_ASCII_testfile_*

printf "%s\n" "Info: Catting all files in ${DESTINATION}:"
find "${DESTINATION}" -type f -print0 | while IFS= read -r -d '' file; do
    printf '\n%s\n' "${file}"
    cat -- "${file}"
done

printf "%s\n" "Info: Total number of created files:"
find "${DESTINATION}" -type f | wc -l

printf "\nInfo: Processing complete.\n"

cleanup

script_name1="$(basename "${0}")"
printf "\nInfo: script_name1: %s\n" "${script_name1}"
script_path1="$(realpath "$(dirname "${0}")")"
printf "Info: script_path1: %s\n" "${script_path1}"
script_path_with_name="${script_path1}/${script_name1}"
printf "Info: Script path with name: %s\n" "${script_path_with_name}"
printf "Info: Script finished\n"
exit 0
