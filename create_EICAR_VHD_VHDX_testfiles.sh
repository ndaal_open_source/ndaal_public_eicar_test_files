#!/usr/bin/env bash
# Author: Pierre Gronau <Pierre.Gronau@ndaal.eu>
# Copyright 2024, 2025
# License: All content is licensed under the terms of the <MIT License>
# Developed on: Debian 12.9x; macOS Sequoia x86 architecture
# Tested on: Debian 12.9x; macOS Sequoia x86 architecture
#
# Note
# This script creates or download files containing the EICAR test string, 
# which may trigger antivirus software. Use with caution and only 
# in controlled environments for legitimate testing purposes.
#
# This Bash script creates VHD (Virtual Hard Disk) and VHDX 
# (Hyper-V Virtual Hard Disk) files containing the 
# EICAR (European Institute for Computer Antivirus Research) test file. 
# These files are commonly used for testing antivirus software and 
# security systems.
# Script Overview
# The script performs the following main tasks:
# - Sets up error handling and cleanup procedures.
# - Creates an EICAR test file.
# - Defines and creates an output directory for the generated files.
# - Checks for required system commands.
# - Creates NTFS-formatted VHD and VHDX files, each containing the EICAR test file.
# - Moves the created files to the output directory.
# Key Features
# - Error Handling: Implements robust error handling and cleanup procedures.
# - EICAR Test File: Generates the standard EICAR test string.
# - File Creation: Creates both VHD and VHDX files formatted with NTFS.
# - File Injection: Injects the EICAR test file into each virtual disk file.
# - Command Verification: Checks for the presence of required system commands.
# Output Management: Creates and manages an output directory for the generated files.
# Usage
# Run the script with appropriate permissions. It will create two files:
# EICAR_VHD_container.vhd
# EICAR_VHDX_container.vhdx
# These files will be placed in the dataset/EICAR_VHD_VHDX_testfiles directory relative to the script's location.
# Requirements
# The script requires the following commands to be available:
# - dd
# - mkntfs
# - qemu-img
# - ntfs-3g
# umount
# Ensure these are installed on your system before running the script.
#
# Exit on error. Append "|| true" if you expect an error.
set -o errexit
# This is equivalent to set -e. It causes the script to exit 
# immediately if any command exits with a non-zero status.
#
# Exit on error inside any functions or subshells.
set -o errtrace
# This setting ensures that the ERR trap is inherited by shell functions,
# command substitutions, and commands executed in a subshell environment.
#
# Do not allow use of undefined vars. Use ${VAR:-} to use an undefined VAR
set -o nounset
# This is equivalent to set -u. It treats unset variables as 
# an error when substituting.
#
# Catch the error in case mysqldump fails (but gzip succeeds) in `mysqldump |gzip`
# https://vaneyckt.io/posts/safer_bash
#set -o pipefail
# This setting causes a pipeline to return the exit status of the last command
# in the pipe that returned a non-zero status.
#
# Turn on traces, useful while debugging but commented out by default
# set -o xtrace
#
# Set $IFS to only newline and tab.
#
# http://www.dwheeler.com/essays/filenames-in-shell.html
# nosemgrep: ifs-tampering
IFS=$'\n\t'

# Error trapping function
# Add this line before the error_handler function
# shellcheck disable=SC2317
error_handler() {
    local line_number="${1}"
    local error_code="${2}"
    local last_command="${BASH_COMMAND}"
    printf "Error: Error occurred in line %s (error code: %s)\n" "${line_number}" "${error_code}"
    printf "Error: Failed command: %s\n" "${last_command}"

    # Success case
    return 0
}

# Set the error trap
trap "error_handler ${LINENO} \$?" ERR

cleanup() {
    trap - SIGINT SIGTERM ERR EXIT
    printf "%b\n" "\nInfo: Cleanup is running ..."
    # Additional cleanup tasks can be added here if needed
    rm -f -v ./*.tmp
    printf "%b\n" "\nInfo: Cleanup finished ..."
}

trap cleanup SIGINT SIGTERM EXIT

# Define the EICAR test string
# shellcheck disable=SC2016
input_string='X5O!P%@AP[4\PZX54(P^)7CC)7}$EICAR-STANDARD-ANTIVIRUS-TEST-FILE!$H+H*'
readonly input_string

eicar_file="eicar.txt"
readonly eicar_file

printf "%s\n" "${input_string}" > "${eicar_file}"

if [ ! -f "${eicar_file}" ]; then
    printf "Error: %s not found. Exiting.\n" "${eicar_file}"
    exit 1
fi

# Define script path and output directory
script_path="${BASH_SOURCE[0]}"
readonly script_path
printf "Info: Current script_path: %s\n" "${script_path}"

script_dir="$(cd "$(dirname "${script_path}")" && pwd)"
readonly script_dir
printf "Info: Current script_dir: %s\n" "${script_dir}"

output_dir="${script_dir}/dataset/EICAR_compressed_nested"
readonly output_dir
printf "Info: Current output_dir: %s\n" "${output_dir}"

# Create output directory if it doesn't exist
if [ ! -d "${output_dir}" ]; then
    if mkdir "${output_dir}"; then
        printf "Info: Created output directory: %s\n" "${output_dir}"
    else
        printf "Error: Failed to create output directory: %s\n" "${output_dir}" >&2
        exit 1
    fi
else
    printf "Info: Output directory already exists: %s\n" "${output_dir}"
fi

# Function to check if a command is available
check_command() {
    if ! command -v "${1}" &> /dev/null; then
        printf "Error: %s is not installed or not in PATH. Please install it and try again.\n" "${1}"
        exit 1
    fi
}

# Check for required commands
check_command "dd" || exit 1
check_command "mkntfs" || exit 1
check_command "qemu-img" || exit 1
check_command "ntfs-3g" || exit 1
check_command "umount" || exit 1

# Function to create an NTFS-formatted file and inject EICAR file
create_ntfs_file() {
    local filename="${1}"
    local size="${2}"
    local eicar_file="${3}"

    # Create a raw file to act as an NTFS volume
    dd if=/dev/zero of="${filename}" bs=1M count="${size}"

    # Format the file with NTFS
    mkntfs -F "${filename}"

    # Mount the NTFS file using ntfs-3g
    mkdir -p -v "/mnt/vhd"
    ntfs-3g "${filename}" "/mnt/vhd"

    # Copy EICAR file into the NTFS volume
    cp -f -p -v "${eicar_file}" "/mnt/vhd/"

    # Unmount and check for successful unmounting
    umount "/mnt/vhd" || { printf "Failed to unmount /mnt/vhd\n"; exit 1; }

    # Ensure directory is empty before removing it
    if [ -d "/mnt/vhd" ] && [ -z "$(ls -A /mnt/vhd)" ]; then
        rmdir "/mnt/vhd"
    else
        printf "Warning: /mnt/vhd is not empty or does not exist.\n"
    fi

    printf "NTFS file created: %s with EICAR file injected\n" "${filename}"
}

# Create VHD and VHDX files (as NTFS-formatted files)
create_ntfs_file "EICAR_VHD_container.vhd" 10 "${eicar_file}"
create_ntfs_file "EICAR_VHDX_container.vhdx" 10 "${eicar_file}"

printf "Info: VHD and VHDX containers created with EICAR test file injected.\n"

mv -v "./EICAR_VHD_container.vhd" "${output_dir}"
mv -v "./EICAR_VHD_container.vhdx" "${output_dir}"

printf "Info: All files have been created and copied in %s.\n" "${output_dir}"

cleanup

script_name1="$(basename "${0}")"
printf "\nInfo: script_name1: %s\n" "${script_name1}"
script_path1="$(realpath "$(dirname "${0}")")"
printf "Info: script_path1: %s\n" "${script_path1}"
script_path_with_name="${script_path1}/${script_name1}"
printf "Info: Script path with name: %s\n" "${script_path_with_name}"
printf "Info: Script finished\n"
exit 0
