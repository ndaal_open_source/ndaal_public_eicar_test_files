#!/usr/bin/env bash
# Author: Pierre Gronau <Pierre.Gronau@ndaal.eu>
# Copyright 2025
# License: All content is licensed under the terms of the <Apache 2.0>
# Developed on: Debian 12.9x; macOS Sequoia x86 architecture
# Tested on: Debian 12.9x; macOS Sequoia x86 architecture
#
set -o errexit
set -o errtrace
set -o nounset
set -o pipefail
# set -o xtrace
# Set IFS explicitly
# nosemgrep: ifs-tampering
IFS=$'\n\t'

#######################################
# GLOBAL VARIABLES
#######################################
VERSION="1.0.0"
readonly VERSION

SCRIPT_NAME="$(basename "${0}")"
readonly SCRIPT_NAME

SCRIPT_DIR="$(realpath "$(dirname "${0}")")"
readonly SCRIPT_DIR

# Default settings
DEFAULT_REQUIRED_SPACE_MB=1000  # Default: 1GB in MB
readonly DEFAULT_REQUIRED_SPACE_MB

DIRDATE="$(date +"%Y-%m-%d")"
readonly DIRDATE

OUTPUT_DIR="."
REPO_PATH=""
RESULTS_FILE=""
VERBOSE=0
QUIET=0
JSON_OUTPUT=0
FORMATS_TO_GENERATE="all" # Comma-separated list or "all"
SEVERITY_FILTER="" # Comma-separated list of severities
REQUIRED_SPACE_MB="${DEFAULT_REQUIRED_SPACE_MB}"
REQUIRED_SPACE_KB=$((REQUIRED_SPACE_MB * 1024))
TEMP_DIR=""

#######################################
# LOGGING FUNCTIONS
#######################################
log_error() {
    printf "ERROR: %s\n" "$*" >&2
}

log_warn() {
    printf "WARNING: %s\n" "$*" >&2
}

log_info() {
    if [[ "${QUIET}" -eq 0 ]]; then
        printf "INFO: %s\n" "$*"
    fi
}

log_debug() {
    if [[ "${VERBOSE}" -ge 1 && "${QUIET}" -eq 0 ]]; then
        printf "DEBUG: %s\n" "$*"
    fi
}

log_success() {
    if [[ "${QUIET}" -eq 0 ]]; then
        printf "SUCCESS: %s\n" "$*"
    fi
}

#######################################
# ERROR HANDLING
#######################################
# Error trapping function
# shellcheck disable=SC2317
error_handler() {
    local line_number="${1}"
    local error_code="${2}"
    local last_command="${BASH_COMMAND}"
    log_error "Error occurred in line ${line_number} (error code: ${error_code})"
    log_error "Failed command: ${last_command}"
    return 0
}

# Set the error trap
trap "error_handler ${LINENO} \$?" ERR

#######################################
# CLEANUP FUNCTION
#######################################
cleanup() {
    trap - SIGINT SIGTERM ERR EXIT
    log_debug "Cleanup is running ..."

    # Remove any temporary files created by this script
    if [[ -n "${TEMP_DIR:-}" && -d "${TEMP_DIR}" ]]; then
        log_debug "Removing temporary directory: ${TEMP_DIR}"
        rm -rf "${TEMP_DIR}"
    fi

    log_debug "Cleanup finished"
    return 0
}

trap cleanup SIGINT SIGTERM EXIT

#######################################
# COMMAND CHECKING
#######################################
check_command() {
    if ! command -v "${1}" &>/dev/null; then
        log_error "${1} is not installed or not in PATH. Please install it and try again."
        return 1
    fi
    log_debug "Command ${1} is available"
    return 0
}

#######################################
# OS DETECTION
#######################################
detect_os() {
    # Detect OS type
    if [[ "$(uname)" == "Darwin" ]]; then
        OS_TYPE="macos"
    elif [[ "$(uname)" == "Linux" ]]; then
        OS_TYPE="linux"
    else
        log_error "Unsupported operating system: $(uname)"
        return 1
    fi
    readonly OS_TYPE
    log_debug "Detected OS: ${OS_TYPE}"

    return 0
}

#######################################
# HELP FUNCTION
#######################################
show_help() {
    printf "%s v%s - Software Bill of Materials (SBOM) Generator and Vulnerability Scanner\n\n" "${SCRIPT_NAME}" "${VERSION}"
    printf "Usage: %s [OPTIONS] REPOSITORY_PATH\n\n" "${SCRIPT_NAME}"
    printf "Description:\n"
    printf "    Generate SBOM and scan for vulnerabilities in the specified repository.\n\n"
    printf "Options:\n"
    printf "    -h, --help                  Show this help message and exit\n"
    printf "    -o, --output-dir DIR        Directory to store results (default: current directory)\n"
    printf "    -f, --formats FORMAT        SBOM format(s) to generate (comma-separated, default: all)\n"
    printf "                                Available: spdx-json,spdx-tag-value,cyclonedx-json,syft-json\n"
    printf "    -s, --severity LEVEL        Filter vulnerabilities by severity (comma-separated)\n"
    printf "                                Available: UNKNOWN,LOW,MEDIUM,HIGH,CRITICAL\n"
    printf "    -j, --json                  Output results in JSON format\n"
    printf "    -v, --verbose               Increase verbosity\n"
    printf "    -q, --quiet                 Suppress non-error output\n"
    printf "    --required-space MB         Required disk space in MB (default: %s)\n" "${DEFAULT_REQUIRED_SPACE_MB}"
    printf "    --version                   Show version information and exit\n\n"
    printf "Examples:\n"
    printf "    %s /path/to/repository\n" "${SCRIPT_NAME}"
    printf "    %s --output-dir /tmp/results --formats spdx-json,cyclonedx-json /path/to/repo\n" "${SCRIPT_NAME}"
    printf "    %s --severity HIGH,CRITICAL --json /path/to/repo\n\n" "${SCRIPT_NAME}"
    printf "Requirements:\n"
    printf "    - syft    (SBOM generator)\n"
    printf "    - grype   (vulnerability scanner)\n"
    printf "    - trivy   (vulnerability scanner)\n\n"
    printf "Report bugs to: Pierre.Gronau@ndaal.eu\n"
}

show_version() {
    printf "%s version %s\n" "${SCRIPT_NAME}" "${VERSION}"
}

#######################################
# CHECK REQUIREMENTS
#######################################
check_requirements() {
    local has_error=0

    # Check commands
    log_info "Checking for required commands..."
    check_command "syft" || has_error=1
    check_command "grype" || has_error=1
    check_command "trivy" || has_error=1

    # Check OS-specific commands
    if [[ "${OS_TYPE}" == "linux" ]]; then
        check_command "realpath" || has_error=1
    fi

    # Validate output directory
    if [[ ! -d "${OUTPUT_DIR}" ]]; then
        log_error "Output directory '${OUTPUT_DIR}' does not exist"
        has_error=1
    elif [[ ! -w "${OUTPUT_DIR}" ]]; then
        log_error "Output directory '${OUTPUT_DIR}' is not writable"
        has_error=1
    fi

    # Validate repository path
    if [[ ! -d "${REPO_PATH}" ]]; then
        log_error "Repository path '${REPO_PATH}' does not exist or is not a directory"
        has_error=1
    elif [[ ! -r "${REPO_PATH}" ]]; then
        log_error "Repository path '${REPO_PATH}' is not readable"
        has_error=1
    fi

    # Exit if any errors
    if [[ "${has_error}" -eq 1 ]]; then
        log_error "One or more requirements not met. Exiting."
        exit 1
    fi

    log_success "All requirements met"
    return 0
}

#######################################
# DISK SPACE CHECK
#######################################
check_disk_space() {
    local available_space

    if [[ "${OS_TYPE}" == "macos" ]]; then
        # On macOS, the df command displays slightly differently
        available_space=$(df -k "${OUTPUT_DIR}" | awk 'NR==2 {print $4}')
    else
        # Linux
        available_space=$(df -k "${OUTPUT_DIR}" | awk 'NR==2 {print $4}')
    fi

    log_info "Checking disk space. Required: ${REQUIRED_SPACE_MB}MB (${REQUIRED_SPACE_KB}KB), Available: $((available_space / 1024))MB"

    if [[ "${available_space}" -lt "${REQUIRED_SPACE_KB}" ]]; then
        log_error "Insufficient disk space!"
        log_error "Required: ${REQUIRED_SPACE_MB}MB (${REQUIRED_SPACE_KB}KB)"
        log_error "Available: $((available_space / 1024))MB (${available_space}KB)"
        return 1
    fi

    # If we have less than 2x the required space, warn the user
    if [[ "${available_space}" -lt "$((REQUIRED_SPACE_KB * 2))" ]]; then
        log_warn "Available disk space is less than 2x the required space"
        log_warn "This might cause issues if large files are generated"
    fi

    log_success "Sufficient disk space available"
    return 0
}

#######################################
# SBOM GENERATION
#######################################
# Define all available SBOM formats
declare -A sbom_formats=(
    ["spdx-json"]="sbom_spdx-json_2.3.json"
    ["spdx-tag-value"]="sbom_spdx-tag-value_2.3.spdx"
    ["spdx-tag-value@2.2"]="sbom_spdx-tag-value_2.2.spdx"
    ["spdx-json@2.2"]="sbom_spdx-json_2.2.json"
    ["cyclonedx-json"]="sbom_cyclonedx-json.json"
    ["syft-json"]="sbom_syft-json.json"
    ["syft-text"]="sbom_syft-text.txt"
    ["syft-table"]="sbom_syft-table.table"
)
readonly sbom_formats

# Generate SBOM in the specified format
generate_sbom() {
    local output_format="${1}"
    local output_file="${2}"
    local output_path="${OUTPUT_DIR}/${output_file}"
    local exclude_patterns=""

    log_info "Generating SBOM in ${output_format} format..."

    # Create exclude patterns for all SBOM files
    for file in "${sbom_formats[@]}"; do
        exclude_patterns+=" --exclude \"**/${file}\""
    done

    # Check if syft is available
    if ! check_command "syft"; then
        log_error "syft command not found, cannot generate SBOM"
        return 1
    fi

    # Generate SBOM
    local cmd="syft dir:\"${REPO_PATH}\" --output ${output_format} ${exclude_patterns} > \"${output_path}\""
    log_debug "Running command: ${cmd}"

    if ! eval "${cmd}"; then
        log_error "Failed to generate SBOM for ${output_file}"
        return 1
    fi

    # Verify the output file exists and has content
    if [[ ! -f "${output_path}" ]]; then
        log_error "Output file ${output_path} was not created"
        return 1
    fi

    if [[ ! -s "${output_path}" ]]; then
        log_error "Output file ${output_path} is empty"
        return 1
    fi

    log_success "Successfully generated SBOM in ${output_format} format: ${output_path}"
    return 0
}

# Generate all requested SBOM formats
generate_sboms() {
    local formats_to_process=()
    local return_code=0

    # Set SYFT_FILE_METADATA_SELECTION environment variable
    export SYFT_FILE_METADATA_SELECTION="all"
    readonly SYFT_FILE_METADATA_SELECTION

    # Determine which formats to generate
    if [[ "${FORMATS_TO_GENERATE}" == "all" ]]; then
        # Use all unique format keys (avoid duplicates)
        # Get keys and remove duplicates
        for format in "${!sbom_formats[@]}"; do
            if [[ ! " ${formats_to_process[*]} " =~ " ${format} " ]]; then
                formats_to_process+=("${format}")
            fi
        done
    else
        # Parse comma-separated list
        IFS=',' read -ra formats_list <<< "${FORMATS_TO_GENERATE}"
        for format in "${formats_list[@]}"; do
            format=$(printf "%s" "${format}" | xargs) # Trim whitespace
            if [[ -n "${format}" ]]; then
                if [[ -n "${sbom_formats[${format}]:-}" ]]; then
                    formats_to_process+=("${format}")
                else
                    log_warn "Unknown format: ${format}, skipping"
                fi
            fi
        done
    fi

    log_info "Will generate the following SBOM formats: ${formats_to_process[*]}"

    # Remove existing SBOM files in output directory
    for file in "${sbom_formats[@]}"; do
        if [[ -f "${OUTPUT_DIR}/${file}" ]]; then
            log_debug "Removing existing SBOM file: ${OUTPUT_DIR}/${file}"
            rm -f "${OUTPUT_DIR}/${file}"
        fi
    done

    # Generate each format
    for format in "${formats_to_process[@]}"; do
        if ! generate_sbom "${format}" "${sbom_formats[${format}]}"; then
            log_error "Failed to generate SBOM for format ${format}"
            return_code=1
        fi
    done

    # Ensure we have syft-json for vulnerability scanning
    local syft_json="${OUTPUT_DIR}/${sbom_formats["syft-json"]}"
    if [[ ! -f "${syft_json}" || ! -s "${syft_json}" ]]; then
        log_warn "syft-json format not found or empty, generating it for vulnerability scanning"
        if ! generate_sbom "syft-json" "${sbom_formats["syft-json"]}"; then
            log_error "Failed to generate syft-json SBOM required for vulnerability scanning"
            return_code=1
        fi
    fi

    return "${return_code}"
}

#######################################
# VULNERABILITY SCANNING
#######################################
# Run Grype vulnerability scanner
run_grype_scan() {
    local syft_json="${OUTPUT_DIR}/${sbom_formats["syft-json"]}"
    local severity_flag=""
    local format_flag=""
    local temp_file=""

    log_info "Scanning for vulnerabilities with Grype..."

    # Check if grype is available
    if ! check_command "grype"; then
        log_error "grype command not found, cannot scan for vulnerabilities"
        return 1
    fi

    # Set severity filter if specified
    if [[ -n "${SEVERITY_FILTER}" ]]; then
        severity_flag="--only-fixed --fail-on ${SEVERITY_FILTER}"
    fi

    # Set output format
    if [[ "${JSON_OUTPUT}" -eq 1 ]]; then
        format_flag="--output json"
    else
        format_flag="--output table"
    fi

    # Run Grype
    local cmd="grype sbom:${syft_json} ${severity_flag} ${format_flag}"
    log_debug "Running command: ${cmd}"

    temp_file=$(mktemp "${TEMP_DIR}/grype_results.XXXXXX")

    if ! eval "${cmd}" > "${temp_file}"; then
        # Note: Grype returns non-zero if vulnerabilities are found, which is expected
        log_warn "Grype found vulnerabilities (or encountered an error)"
    fi

    # Add header and append results to the main results file
    {
        if [[ "${JSON_OUTPUT}" -eq 1 ]]; then
            cat "${temp_file}"
        else
            printf "=== Grype Scan Results ===\n\n"
            cat "${temp_file}"
            printf "\n\n"
        fi
    } >> "${RESULTS_FILE}"

    log_success "Grype scan completed"
    return 0
}

# Run Trivy vulnerability scanner
run_trivy_scan() {
    local severity_flag=""
    local format_flag=""
    local temp_file=""
    local trivy_output=""

    log_info "Scanning for vulnerabilities with Trivy..."

    # Check if trivy is available
    if ! check_command "trivy"; then
        log_error "trivy command not found, cannot scan for vulnerabilities"
        return 1
    fi

    # Set up Trivy cache directory
    TRIVY_CACHE_DIR="${HOME}/.cache/trivy"
    readonly TRIVY_CACHE_DIR

    mkdir -p "${TRIVY_CACHE_DIR}"

    # Update Trivy database if needed
    if [[ ! -f "${TRIVY_CACHE_DIR}/trivy.db" ]] || [[ -z "$(find "${TRIVY_CACHE_DIR}/trivy.db" -mtime -1 2>/dev/null)" ]]; then
        log_info "Updating Trivy database..."
        for i in {1..3}; do
            if trivy --cache-dir "${TRIVY_CACHE_DIR}" image --download-db-only; then
                log_success "Trivy database updated successfully"
                break
            fi
            log_warn "Database update failed, retrying in $((2**i)) seconds..."
            sleep $((2**i))
            if [[ "${i}" -eq 3 ]]; then
                log_error "Failed to update Trivy database after 3 attempts"
                return 1
            fi
        done
    else
        log_debug "Using existing Trivy database (less than 1 day old)"
    fi

    # Set severity filter if specified
    if [[ -n "${SEVERITY_FILTER}" ]]; then
        severity_flag="--severity ${SEVERITY_FILTER}"
    fi

    # Set output format
    if [[ "${JSON_OUTPUT}" -eq 1 ]]; then
        format_flag="--format json"
    fi

    # Run Trivy
    local cmd="trivy --cache-dir \"${TRIVY_CACHE_DIR}\" fs ${format_flag} ${severity_flag} -v --scanners vuln,misconfig,secret \"${REPO_PATH}\""
    log_debug "Running command: ${cmd}"

    temp_file=$(mktemp "${TEMP_DIR}/trivy_results.XXXXXX")

    if ! eval "${cmd}" > "${temp_file}" 2>/dev/null; then
        log_warn "Trivy found vulnerabilities (or encountered an error)"
    fi

    # Add header and append results to the main results file
    {
        if [[ "${JSON_OUTPUT}" -eq 1 ]]; then
            cat "${temp_file}"
        else
            printf "=== Trivy Scan Results ===\n\n"
            trivy_output=$(cat "${temp_file}")
            if [[ -z "${trivy_output}" ]]; then
                printf "No vulnerabilities found by Trivy.\n"
            else
                printf "%s\n" "${trivy_output}"
            fi
            printf "\n\n"
        fi
    } >> "${RESULTS_FILE}"

    log_success "Trivy scan completed"
    return 0
}

#######################################
# PARSE ARGUMENTS
#######################################
parse_arguments() {
    # If no arguments provided, show help
    if [[ $# -eq 0 ]]; then
        show_help
        exit 0
    fi

    # Parse options
    while [[ $# -gt 0 ]]; do
        case "${1}" in
            -h|--help)
                show_help
                exit 0
                ;;
            --version)
                show_version
                exit 0
                ;;
            -o|--output-dir)
                if [[ -n "${2:-}" ]]; then
                    OUTPUT_DIR="${2}"
                    shift 2
                else
                    log_error "--output-dir requires a directory path"
                    exit 1
                fi
                ;;
            -f|--formats)
                if [[ -n "${2:-}" ]]; then
                    FORMATS_TO_GENERATE="${2}"
                    shift 2
                else
                    log_error "--formats requires a comma-separated list of formats"
                    exit 1
                fi
                ;;
            -s|--severity)
                if [[ -n "${2:-}" ]]; then
                    SEVERITY_FILTER="${2}"
                    shift 2
                else
                    log_error "--severity requires a comma-separated list of severities"
                    exit 1
                fi
                ;;
            -j|--json)
                JSON_OUTPUT=1
                shift
                ;;
            -v|--verbose)
                VERBOSE=$((VERBOSE + 1))
                shift
                ;;
            -q|--quiet)
                QUIET=1
                shift
                ;;
            --required-space)
                if [[ -n "${2:-}" && "${2}" =~ ^[0-9]+$ ]]; then
                    REQUIRED_SPACE_MB="${2}"
                    shift 2
                else
                    log_error "--required-space requires a number in MB"
                    exit 1
                fi
                ;;
            -*)
                log_error "Unknown option: ${1}"
                show_help
                exit 1
                ;;
            *)
                # Assume it's the repository path
                if [[ -z "${REPO_PATH:-}" ]]; then
                    REPO_PATH="${1}"
                    shift
                else
                    log_error "Repository path already specified: ${REPO_PATH}"
                    log_error "Unknown argument: ${1}"
                    show_help
                    exit 1
                fi
                ;;
        esac
    done

    # Validate required parameters
    if [[ -z "${REPO_PATH:-}" ]]; then
        log_error "Repository path is required"
        show_help
        exit 1
    fi

    # Make variables readonly after they are set from command line arguments
    #readonly OUTPUT_DIR
    #readonly REPO_PATH
    #readonly FORMATS_TO_GENERATE
    #readonly SEVERITY_FILTER
    #readonly JSON_OUTPUT
    #readonly VERBOSE
    #readonly QUIET
    #readonly REQUIRED_SPACE_MB

    # Create absolute paths
    if check_command "realpath"; then
        REPO_PATH=$(realpath "${REPO_PATH}")
        OUTPUT_DIR=$(realpath "${OUTPUT_DIR}")
    else
        # Fallback for macOS which might not have realpath
        REPO_PATH=$(cd "$(dirname "${REPO_PATH}")" && pwd)/$(basename "${REPO_PATH}")
        OUTPUT_DIR=$(cd "${OUTPUT_DIR}" && pwd)
    fi

    # Set up results file name with date
    if [[ "${JSON_OUTPUT}" -eq 1 ]]; then
        RESULTS_FILE="${OUTPUT_DIR}/sbom_scan_results_${DIRDATE}.json"
    else
        RESULTS_FILE="${OUTPUT_DIR}/sbom_scan_results_${DIRDATE}.txt"
    fi
    readonly RESULTS_FILE

    # Convert MB to KB for df comparison
    REQUIRED_SPACE_KB=$((REQUIRED_SPACE_MB * 1024))
    readonly REQUIRED_SPACE_KB

    # Create temp directory with secure permissions
    if check_command "mktemp"; then
        TEMP_DIR=$(mktemp -d)
    else
        # Fallback for systems without mktemp
        TEMP_DIR="/tmp/sbom_scan_${RANDOM}_${RANDOM}"
        mkdir -p "${TEMP_DIR}"
        chmod 700 "${TEMP_DIR}"
    fi

    if [[ ! -d "${TEMP_DIR}" ]]; then
        log_error "Failed to create temporary directory"
        exit 1
    fi
    readonly TEMP_DIR

    log_debug "Repository path: ${REPO_PATH}"
    log_debug "Output directory: ${OUTPUT_DIR}"
    log_debug "Results file: ${RESULTS_FILE}"
    log_debug "Required space: ${REQUIRED_SPACE_MB}MB"
    log_debug "Formats to generate: ${FORMATS_TO_GENERATE}"
    log_debug "Severity filter: ${SEVERITY_FILTER}"
    log_debug "JSON output: ${JSON_OUTPUT}"
    log_debug "Verbosity level: ${VERBOSE}"
    log_debug "Quiet mode: ${QUIET}"

    return 0
}

#######################################
# MAIN FUNCTION
#######################################
main() {
    # Detect OS first
    detect_os || exit 1

    # Parse command line arguments
    parse_arguments "$@"

    # Check requirements
    check_requirements

    # Check disk space
    check_disk_space || exit 1

    # Clean up any existing results file
    if [[ -f "${RESULTS_FILE}" ]]; then
        log_debug "Removing existing results file: ${RESULTS_FILE}"
        rm -f "${RESULTS_FILE}"
    fi

    # Generate SBOMs
    log_info "Generating Software Bill of Materials (SBOM)..."
    if ! generate_sboms; then
        log_error "SBOM generation failed"
        exit 1
    fi

    # Initialize results file with header
    if [[ "${JSON_OUTPUT}" -eq 1 ]]; then
        printf "{\n" > "${RESULTS_FILE}"
        printf "  \"scanDate\": \"%s\",\n" "$(date -u +"%Y-%m-%dT%H:%M:%SZ")" >> "${RESULTS_FILE}"
        printf "  \"repository\": \"%s\",\n" "${REPO_PATH}" >> "${RESULTS_FILE}"
        printf "  \"scanResults\": {\n" >> "${RESULTS_FILE}"
    else
        {
            printf "=============================================\n"
            printf "SBOM and Vulnerability Scan Results\n"
            printf "=============================================\n"
            printf "Date: %s\n" "$(date)"
            printf "Repository: %s\n" "${REPO_PATH}"
            printf "=============================================\n\n"
        } > "${RESULTS_FILE}"
    fi

    # Run vulnerability scans
    log_info "Running vulnerability scans..."

    # Run Grype scan
    run_grype_scan

    # Run Trivy scan
    run_trivy_scan

    # Finalize JSON if needed
    if [[ "${JSON_OUTPUT}" -eq 1 ]]; then
        printf "  }\n" >> "${RESULTS_FILE}"
        printf "}\n" >> "${RESULTS_FILE}"
    fi

    log_success "Vulnerability scans completed. Results saved to ${RESULTS_FILE}"

    return 0
}

# Run the main function
main "$@"

log_debug "Script finished successfully"

cleanup

script_name1="$(basename "${0}")"
printf "\nInfo: script_name1: %s\n" "${script_name1}"
script_path1="$(realpath "$(dirname "${0}")")"
printf "Info: script_path1: %s\n" "${script_path1}"
script_path_with_name="${script_path1}/${script_name1}"
printf "Info: Script path with name: %s\n" "${script_path_with_name}"
printf "Info: Script finished\n"
exit 0
