#!/usr/bin/env bash
# Author: Pierre Gronau <Pierre.Gronau@ndaal.eu>
# Copyright 2024, 2025
# License: All content is licensed under the terms of the <MIT License>
# Developed on: Debian 12.x; macOS Sequoia x86 architecture
# Tested on: Debian 12.x; macOS Sequoia x86 architecture
#
# Exit on error. Append "|| true" if you expect an error.
set -o errexit
# This is equivalent to set -e. It causes the script to exit 
# immediately if any command exits with a non-zero status.
#
# Exit on error inside any functions or subshells.
set -o errtrace
# This setting ensures that the ERR trap is inherited by shell functions,
# command substitutions, and commands executed in a subshell environment.
#
# Do not allow use of undefined vars. Use ${VAR:-} to use an undefined VAR
set -o nounset
# This is equivalent to set -u. It treats unset variables as 
# an error when substituting.
#
# Catch the error in case mysqldump fails (but gzip succeeds) in `mysqldump |gzip`
# https://vaneyckt.io/posts/safer_bash
set -o pipefail
# This setting causes a pipeline to return the exit status of the last command
# in the pipe that returned a non-zero status.
#
# Turn on traces, useful while debugging but commented out by default
# set -o xtrace

# Set $IFS to only newline and tab.
#
# http://www.dwheeler.com/essays/filenames-in-shell.html
# nosemgrep: ifs-tampering
IFS=$'\n\t'

# Error trapping function
# Add this line before the error_handler function
# shellcheck disable=SC2317
error_handler() {
    local line_number="${1}"
    local error_code="${2}"
    local last_command="${BASH_COMMAND}"
    printf "Error: Error occurred in line %s (error code: %s)\n" "${line_number}" "${error_code}"
    printf "Error: Failed command: %s\n" "${last_command}"

    # Success case
    return 0
}

# Set the error trap
trap "error_handler ${LINENO} \$?" ERR

cleanup() {
    trap - SIGINT SIGTERM ERR EXIT
    printf "%b\n" "\nInfo: Cleanup is running ..."
    # Additional cleanup tasks can be added here if needed
    rm -f -v ./*.tmp
    printf "%b\n" "\nInfo: Cleanup finished ..."
}

trap cleanup SIGINT SIGTERM EXIT

# Function to check if running as root
check_root() {
    if [ "$(id -u)" -ne 0 ]; then
        printf "This script must be run as root or with sudo privileges. Exiting.\n" >&2
        return 1
    fi
}

# Determine the operating system and check for root privileges
if [[ "${OSTYPE}" == "darwin"* ]]; then
    # macOS
    check_root
    OS="macOS"
elif [[ "${OSTYPE}" == "linux-gnu"* ]]; then
    # Linux
    check_root
    OS="Linux"
else
    printf "Unsupported operating system: %s\n" "${OSTYPE}" >&2
    return 1
fi

printf "Info: Running as root on %s.\n" "${OS}"
printf "Info: Continuing with script execution.\n"

# Print additional information
printf "Info: Script started. Running as user: %s\n" "$(whoami)"
printf "Info: Current working directory: %s\n" "$(pwd)"

# Function to check if RAMDisk exists and exit if it does
check_ramdisk_and_exit() {
    local ramdisk_path="$1"
    if [ -d "$ramdisk_path" ]; then
        printf "Warning: RAMDisk already exists at %s. Exiting.\n" "$ramdisk_path"
        return 1
    fi

    # Success case
    return 0
}

# Determine the operating system
if [[ "$OSTYPE" == "darwin"* ]]; then
    # macOS
    check_ramdisk_and_exit "/Volumes/RAMDisk"
elif [[ "$OSTYPE" == "linux-gnu"* ]]; then
    # Linux
    check_ramdisk_and_exit "/tmp/RAMDisk"
else
    printf "Error: Unsupported operating system: %s\n" "$OSTYPE"
    exit 1
fi

printf "Info: RAMDisk does not exist. Continuing with script execution.\n"

create_linux_ramdisk() {
    local size=$1 # Size in MB
    local mount_point="/tmp/ramdisk"
    sudo mkdir -p "${mount_point}"
    sudo mount -t tmpfs -o size="${size}M" tmpfs "${mount_point}"
    printf "Info: RAM disk created at %s with size %sMB\n" "${mount_point}" "${size}"
}

create_macos_ramdisk() {
    local size=$1 # Size in MB
    local sectors=$((size * 2048)) # Convert MB to 512-byte sectors
    local mount_point="/Volumes/RAMDisk"
    
    printf "Info: Attempting to create a %sMB RAM disk...\n" "${size}"
    
    # Create the RAM disk
    local disk_identifier=$(hdiutil attach -nomount ram://${sectors})
    if [ $? -ne 0 ]; then
        printf "Error: Failed to create RAM disk.\n"
        return 1
    fi
    disk_identifier=$(printf "%s" "$disk_identifier" | tr -d ' ') # Remove any whitespace
    printf "Info: RAM disk created with identifier: %s\n" "${disk_identifier}"
    
    # Wait a moment for the disk to be recognized by the system
    sleep 2
    
    # Format the disk
    if ! diskutil partitionDisk ${disk_identifier} 1 GPT APFS 'RAMDisk' '100%'; then
        printf "Error: Failed to format RAM disk.\n"
        hdiutil detach "${disk_identifier}"
        return 1
    fi
    
    printf "Info: RAM disk created and mounted at %s with size %sMB\n" "${mount_point}" "${size}"
    printf "Info: Disk identifier: %s\n" "${disk_identifier}"
}

usage() {
    printf "Info: Usage: %s <size_in_mb> <os>\n" "$0"
    printf "Info: Example for Linux: %s 512 linux\n" "$0"
    printf "Info: Example for macOS: %s 512 macos\n" "$0"
    exit 1
}

# Check if correct number of arguments are provided
if [ "$#" -ne 2 ]; then
    usage
fi

size=$1
os=$2

case "${os}" in
    linux)
        create_linux_ramdisk "${size}"
        ;;
    macos)
        # Just before calling create_macos_ramdisk
        printf "Info: Attempting to create macOS RAM disk of size %sMB\n" "${size}"
        create_macos_ramdisk "${size}"
        # After calling create_macos_ramdisk
        if [ $? -ne 0 ]; then
            printf "Info: RAM disk creation failed. Please check system resources and permissions.\n"
            exit 1
        fi
        touch "/Volumes/RAMDisk/test.txt"
        ls -l "/Volumes/RAMDisk/test.txt"
        ;;
    *)
        printf "Error: Unsupported OS. Use 'linux' or 'macos'.\n"
        usage
        ;;
esac

printf "\nInfo: Script finished successfully.\n"

cleanup

script_name1="$(basename "${0}")"
printf "\nInfo: script_name1: %s\n" "${script_name1}"
script_path1="$(realpath "$(dirname "${0}")")"
printf "Info: script_path1: %s\n" "${script_path1}"
script_path_with_name="${script_path1}/${script_name1}"
printf "Info: Script path with name: %s\n" "${script_path_with_name}"
printf "Info: Script finished\n"
exit 0
