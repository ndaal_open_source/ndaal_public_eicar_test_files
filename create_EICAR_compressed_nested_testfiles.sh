#!/usr/bin/env bash
# Author: Pierre Gronau <Pierre.Gronau@ndaal.eu>
# Copyright 2024, 2025
# License: All content is licensed under the terms of the <MIT License>
# Developed on: Debian 12.8x; macOS Sequoia x86 architecture
# Tested on: Debian 12.8x; macOS Sequoia x86 architecture
#
# Note
# This script creates or download files containing the EICAR test string,
# which may trigger antivirus software. Use with caution and only
# in controlled environments for legitimate testing purposes.
#
# Exit on error. Append "|| true" if you expect an error.
set -o errexit
# This is equivalent to set -e. It causes the script to exit
# immediately if any command exits with a non-zero status.
#
# Exit on error inside any functions or subshells.
set -o errtrace
# This setting ensures that the ERR trap is inherited by shell functions,
# command substitutions, and commands executed in a subshell environment.
#
# Do not allow use of undefined vars. Use ${VAR:-} to use an undefined VAR
set -o nounset
# This is equivalent to set -u. It treats unset variables as
# an error when substituting.
#
# Catch the error in case mysqldump fails (but gzip succeeds) in `mysqldump |gzip`
# https://vaneyckt.io/posts/safer_bash
set -o pipefail
# This setting causes a pipeline to return the exit status of the last command
# in the pipe that returned a non-zero status.
#
# Turn on traces, useful while debugging but commented out by default
# set -o xtrace

# Set $IFS to only newline and tab.
#
# http://www.dwheeler.com/essays/filenames-in-shell.html
# nosemgrep: ifs-tampering
IFS=$'\n\t'

# Error trapping function
# Add this line before the error_handler function
# shellcheck disable=SC2317
error_handler() {
    local line_number="${1}"
    local error_code="${2}"
    local last_command="${BASH_COMMAND}"
    printf "Error: Error occurred in line %s (error code: %s)\n" "${line_number}" "${error_code}"
    printf "Error: Failed command: %s\n" "${last_command}"

    # Success case
    return 0
}

# Set the error trap
trap "error_handler ${LINENO} \$?" ERR

cleanup() {
    trap - SIGINT SIGTERM ERR EXIT
    printf "%b\n" "\nInfo: Cleanup is running"
    # Additional cleanup tasks can be added here if needed
    rm -f -v ./*.tmp
    rm -f -v "./EICAR_compressed_iteration_"*.zip
    rm -f -v "./EICAR_compressed_iteration_"*.7z
    printf "%b\n" "\nInfo: Cleanup finished ..."
}

trap cleanup SIGINT SIGTERM EXIT

# Define the EICAR test string
# shellcheck disable=SC2016
input_string='X5O!P%@AP[4\PZX54(P^)7CC)7}$EICAR-STANDARD-ANTIVIRUS-TEST-FILE!$H+H*'
readonly input_string

# Define the EICAR file name
eicar_file="eicar.txt"
readonly eicar_file

# Create a text file with the EICAR test string and ensure it ends with a newline
printf "%s\n" "${input_string}" > "${eicar_file}"

if [ ! -f "${eicar_file}" ]; then
    printf "Error: %s not found. Exiting.\n" "${eicar_file}"
    exit 1
fi

# Define script path and output directory
script_path="${BASH_SOURCE[0]}"
readonly script_path
printf "Info: Current script_path: %s\n" "${script_path}"

script_dir="$(cd "$(dirname "${script_path}")" && pwd)"
readonly script_dir
printf "Info: Current script_dir: %s\n" "${script_dir}"

output_dir="${script_dir}/dataset/EICAR_compressed_nested"
readonly output_dir
printf "Info: Current output_dir: %s\n" "${output_dir}"

# Create output directory if it doesn't exist
if [ ! -d "${output_dir}" ]; then
    if mkdir "${output_dir}"; then
        printf "Info: Created output directory: %s\n" "${output_dir}"
    else
        printf "Error: Failed to create output directory: %s\n" "${output_dir}" >&2
        exit 1
    fi
else
    printf "Info: Output directory already exists: %s\n" "${output_dir}"
fi

# Create output directory if it doesn't exist
if [ ! -d "${output_dir}/zip" ]; then
    if mkdir "${output_dir}/zip"; then
        printf "Info: Created output directory: %s\n" "${output_dir}/zip"
    else
        printf "Error: Failed to create output directory: %s\n" "${output_dir}/zip" >&2
        exit 1
    fi
else
    printf "Info: Output directory already exists: %s\n" "${output_dir}/zip"
fi

# Create output directory if it doesn't exist
if [ ! -d "${output_dir}/7zip" ]; then
    if mkdir "${output_dir}/7zip"; then
        printf "Info: Created output directory: %s\n" "${output_dir}/7zip"
    else
        printf "Error: Failed to create output directory: %s\n" "${output_dir}/7zip" >&2
        exit 1
    fi
else
    printf "Info: Output directory already exists: %s\n" "${output_dir}/7zip"
fi

# Function to compress the file using zip
compress_file_zip() {
    local iterations="${1}"
    local current_file="eicar.txt"

    for ((i=1; i<=iterations; i++)); do
        # shellcheck disable=SSC2168
        local zip_file="EICAR_compressed_iteration_${i}.zip"
        zip -q "${zip_file}" "${current_file}"
        current_file="${zip_file}"
    done
}

# Function to compress the file using 7zip
compress_file_7zip() {
    local iterations=${1}"
    local current_file="eicar.txt"

    for ((i=1; i<=iterations; i++)); do
        # shellcheck disable=SSC2168
        local zip_file="EICAR_compressed_iteration_${i}.7z"
        7z a -bsp0 -bso0 "${zip_file}" "${current_file}" >/dev/null
        current_file="${zip_file}"
    done
}

# Main execution
main() {
    local compression_iterations=1000  # Change this value to set the number of compressions

    printf "Info: Creating EICAR test file...\n"
    
    printf "Info: Compressing EICAR file %d times using zip...\n" "${compression_iterations}"
    compress_file_zip "${compression_iterations}"
    printf "Info: Zip compression complete. Created files:\n"
    ls -la "./EICAR_compressed_iteration_"*.zip

    printf "\nCompressing EICAR file %d times using 7zip...\n" "${compression_iterations}"
    compress_file_7zip "${compression_iterations}"
    printf "Info: 7zip compression complete. Created files:\n"
    ls -la "./EICAR_compressed_iteration_"*.7z
}

main "$@"

mv -v "./EICAR_compressed_iteration_"*.zip "${output_dir}/zip"
mv -v "./EICAR_compressed_iteration_"*.7z "${output_dir}/7zip"

printf "\nInfo: Processing complete.\n"

cleanup

script_name1="$(basename "${0}")"
printf "\nInfo: script_name1: %s\n" "${script_name1}"
script_path1="$(realpath "$(dirname "${0}")")"
printf "Info: script_path1: %s\n" "${script_path1}"
script_path_with_name="${script_path1}/${script_name1}"
printf "Info: Script path with name: %s\n" "${script_path_with_name}"
printf "Info: Script finished\n"
exit 0
