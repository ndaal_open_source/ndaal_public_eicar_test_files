# archive_samples_eicar

This is a repository of a single eicar.txt file archived in multiple formats and using several different tools.
It was orginally created to test archive inspection limitations of common security tools.

This will grow, I plan to add more formats and operating systems, and increase complexity. 

Please feel free to add your own, The eicar.txt file is on the main page.

Cheers
