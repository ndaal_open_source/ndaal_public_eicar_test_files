#!/usr/bin/env bash
# Author: Pierre Gronau <Pierre.Gronau@ndaal.eu>
# Copyright 2024, 2025
# License: All content is licensed under the terms of the <Apache 2.0>
# Developed on: Debian 12.9x; macOS Sequoia x86 architecture
# Tested on: Debian 12.9x; macOS Sequoia x86 architecture
#
# Note
# This script creates or download files containing the EICAR test string,
# which may trigger antivirus software. Use with caution and only
# in controlled environments for legitimate testing purposes.
#
# Exit on error. Append "|| true" if you expect an error.
set -o errexit
# This is equivalent to set -e. It causes the script to exit
# immediately if any command exits with a non-zero status.
#
# Exit on error inside any functions or subshells.
set -o errtrace
# This setting ensures that the ERR trap is inherited by shell functions,
# command substitutions, and commands executed in a subshell environment.
#
# Do not allow use of undefined vars. Use ${VAR:-} to use an undefined VAR
set -o nounset
# This is equivalent to set -u. It treats unset variables as
# an error when substituting.
#
# Catch the error in case mysqldump fails (but gzip succeeds) in `mysqldump |gzip`
# https://vaneyckt.io/posts/safer_bash
set -o pipefail
# This setting causes a pipeline to return the exit status of the last command
# in the pipe that returned a non-zero status.
#
# Turn on traces, useful while debugging but commented out by default
# set -o xtrace

# Set $IFS to only newline and tab.
#
# http://www.dwheeler.com/essays/filenames-in-shell.html
# nosemgrep: ifs-tampering
IFS=$'\n\t'

# Error trapping function
# Add this line before the error_handler function
# shellcheck disable=SC2317
error_handler() {
    local line_number="${1}"
    local error_code="${2}"
    local last_command="${BASH_COMMAND}"
    printf "Error: Error occurred in line %s (error code: %s)\n" "${line_number}" "${error_code}"
    printf "Error: Failed command: %s\n" "${last_command}"

    # Success case
    return 0
}

# Set the error trap
trap "error_handler ${LINENO} \$?" ERR

cleanup() {
    trap - SIGINT SIGTERM ERR EXIT
    printf "%b\n" "\nInfo: Cleanup is running ..."
    # Additional cleanup tasks can be added here if needed
    rm -f -v "./EICAR_magic_byte_testfile_"*
    rm -f -v "./EICAR_magic_byte_mime_type.csv"
    printf "%b\n" "\nInfo: Cleanup finished ..."
}

trap cleanup SIGINT SIGTERM ERR EXIT

printf "Warning: Remove any existing test file.\n"
rm -f -v "EICAR_magic_byte_testfile"*

# Define the EICAR test string
# shellcheck disable=SC2016
input_string='X5O!P%@AP[4\PZX54(P^)7CC)7}$EICAR-STANDARD-ANTIVIRUS-TEST-FILE!$H+H*'
readonly input_string

eicar_file="eicar.txt"
readonly eicar_file

printf "%s\n" "${input_string}" > "${eicar_file}"

if [ ! -f "${eicar_file}" ]; then
    printf "Error: %s not found. Exiting.\n" "${eicar_file}"
    exit 1
fi

# Define script path and output directory
script_path="${BASH_SOURCE[0]}"
readonly script_path
printf "Info: Current script_path: %s\n" "${script_path}"

script_dir="$(cd "$(dirname "${script_path}")" && pwd)"
readonly script_dir
printf "Info: Current script_dir: %s\n" "${script_dir}"

output_dir="${script_dir}/dataset/EICAR_compressed_nested"
readonly output_dir
printf "Info: Current output_dir: %s\n" "${output_dir}"

# Create output directory if it doesn't exist
if [ ! -d "${output_dir}" ]; then
    if mkdir "${output_dir}"; then
        printf "Info: Created output directory: %s\n" "${output_dir}"
    else
        printf "Error: Failed to create output directory: %s\n" "${output_dir}" >&2
        exit 1
    fi
else
    printf "Info: Output directory already exists: %s\n" "${output_dir}"
fi

# Define magic bytes and their corresponding MIME types
declare -A magic_bytes=(
    ["23 21"]="sh"
    ["02 00 5A 57 52 54"]="cwk"
    ["00 00 02 00 06 04 06 00"]="wk1"
    ["00 00 1A 00 00 10 04 00"]="wk3"
    ["00 00 1A 00 02 10 04 00"]="wk4"
    ["00 00 1A 00 05 10 04"]="123"
    ["00 00 03 F3"]="amiga"
    ["00 00 49 49 58 50 52"]="qxd"
    ["50 57 53 33"]="psafe3"
    ["D4 C3 B2 A1"]="pcap"
    ["4D 3C B2 A1"]="pcap"
    ["0A 0D 0D 0A"]="pcapng"
    ["ED AB EE DB"]="rpm"
    ["53 51 4C 69 74 65 20 66"]="sqlite"
    ["53 50 30 31"]="bin"
    ["49 57 41 44"]="wad"
    ["00"]="pic"
    ["BE BA FE CA"]="dba"
    ["00 01 42 44"]="dba"
    ["00 01 44 54"]="tda"
    ["54 44 46 24"]="tdf"
    ["54 44 45 46"]="tdef"
    ["00 01 00 00"]="palm"
    ["00 00 01 00"]="ico"
    ["69 63 6E 73"]="icns"
    ["66 74 79 70 33 67"]="3gp"
    ["66 74 79 70 68 65 69 63"]="heic"
    ["1F 9D"]="z"
    ["1F A0"]="z"
    ["2D 68 6C 30 2D"]="lzh"
    ["42 41 43 4B 4D 49 4B 45"]="bac"
    ["49 4E 44 58"]="idx"
    ["62 70 6C 69 73 74"]="plist"
    ["42 5A 68"]="bz2"
    ["47 49 46 38 37 61"]="gif"
    ["47 49 46 38 39 61"]="gif"
    ["49 49 2A 00"]="tif"
    ["4D 4D 00 2A"]="tif"
    ["49 49 2A 00 10 00 00 00"]="cr2"
    ["80 2A 5F D7"]="cin"
    ["52 4E 43 01"]="rnc"
    ["4E 55 52 55 49 4D 47"]="nui"
    ["53 44 50 58"]="dpx"
    ["76 2F 31 01"]="exr"
    ["42 50 47 FB"]="bpg"
    ["FF D8 FF DB"]="jpg"
    ["FF D8 FF E0 00 10 4A 46"]="jpg"
    ["FF D8 FF EE"]="jpg"
    ["FF D8 FF E1"]="jpg"
    ["00 00 00 0C 6A 50 20 20"]="jp2"
    ["FF 4F FF 51"]="jp2"
    ["71 6F 69 66"]="qoi"
    ["46 4F 52 4D"]="ilbm"
    ["4C 5A 49 50"]="lz"
    ["30 37 30 37 30 37"]="cpio"
    ["4D 5A"]="exe"
    ["53 4D 53 4E 46 32 30 30"]="ssp"
    ["5A 4D"]="exe"
    ["50 4B 03 04"]="zip"
    ["52 61 72 21 1A 07 00"]="rar"
    ["52 61 72 21 1A 07 01 00"]="rar"
    ["7F 45 4C 46"]="elf"
    ["89 50 4E 47 0D 0A 1A 0A"]="png"
    ["0E 03 13 01"]="hdf4"
    ["89 48 44 46 0D 0A 1A 0A"]="hdf5"
    ["C9"]="com"
    ["CA FE BA BE"]="class"
    ["EF BB BF"]="txt"
    ["FF FE"]="txt"
    ["FE FF"]="txt"
    ["FF FE 00 00"]="txt"
    ["00 00 FE FF"]="txt"
    ["2B 2F 76 38"]="txt"
    ["0E FE FF"]="txt"
    ["DD 73 66 73"]="txt"
    ["FE ED FA CE"]="macho"
    ["FE ED FA CF"]="macho"
    ["FE ED FE ED"]="jks"
    ["CE FA ED FE"]="macho"
    ["CF FA ED FE"]="macho"
    ["25 21 50 53"]="ps"
    ["25 50 44 46 2D"]="pdf"
    ["30 26 B2 75 8E 66 CF 11"]="asf"
    ["24 53 44 49 30 30 30 31"]="sdi"
    ["4F 67 67 53"]="ogg"
    ["38 42 50 53"]="psd"
    ["52 49 46 46"]="avi"
    ["FF FB"]="mp3"
    ["49 44 33"]="mp3"
    ["42 4D"]="bmp"
    ["43 44 30 30 31"]="iso"
    ["4C 00 00 00 01 14 02 00"]="lnk"
    ["62 6F 6F 6B 00 00 00 00"]="alias"
    ["75 73 74 61 72 00 30 30"]="tar"
    ["4D 53 43 46"]="cab"
    ["4B 44 4D"]="vmdk"
    ["43 72 32 34"]="crx"
    ["41 47 44 33"]="fh8"
    ["05 07 00 00 42 4F 42 4F"]="cwk"
    ["00 00 00 14 66 74 79 70"]="mov"
    ["4D 54 68 64"]="mid"
    ["4D 41 52 31 00"]="mar"
    ["4E 45 53 1A"]="nes"
    ["75 73 74 61 72"]="tar"
)

change_magic_byte() {
    local magic_byte="${1}"
    local extension="${2}"
    local new_file="EICAR_magic_byte_testfile_${magic_byte// /_}.${extension}"
    
    # Copy the original file
    if ! cp -v -p eicar.txt "${new_file}"; then
        printf "Error: Failed to copy eicar.txt to %s.\n" "${new_file}"
        return 1
    fi
    
    # Replace the beginning with the magic byte
    if ! printf '%b' "$(printf '\\x%s' "${magic_byte//[[:space:]]/\\x}")" | dd of="${new_file}" bs=1 count="${#magic_byte}" conv=notrunc 2>/dev/null; then
        printf "Error: Failed to write magic bytes to %s.\n" "${new_file}"
        return 1
    fi
    
    # Ensure the file ends with a newline
    if ! sed -i.bak '$a\' "${new_file}"; then
        printf "Error: Failed to add newline to %s.\n" "${new_file}"
        return 1
    fi
    rm -f "${new_file}.bak"
    
    # Check if the file was created successfully
    if [ ! -f "${new_file}" ]; then
        printf "Error: %s was not created.\n" "${new_file}"
        return 1
    fi
    
    printf '%s' "${new_file}"
}

# Process all magic bytes
for magic_byte in "${!magic_bytes[@]}"; do
    extension="${magic_bytes[$magic_byte]}"
    new_file=$(change_magic_byte "${magic_byte}" "${extension}")
    
    if [ $? -ne 0 ]; then
        printf "Error: Failed to process magic byte %s.\n" "${magic_byte}"
        continue
    fi
    
    # Get file information
    if [ ! -f "${new_file}" ]; then
        printf "Error: %s does not exist.\n" "${new_file}"
        continue
    fi
    
    size=$(wc -c < "${new_file}" 2>/dev/null)
    if [ $? -ne 0 ]; then
        printf "Error: Failed to get file size for %s.\n" "${new_file}"
        continue
    fi
    
    mime_type=$(file -b --mime-type "${new_file}" 2>/dev/null)
    if [ $? -ne 0 ]; then
        printf "Error: Failed to get MIME type for %s.\n" "${new_file}"
        continue
    fi
    
    # Append information to CSV
    printf '%s,%s,%s,%s\n' "${new_file}" "${size}" "${magic_byte}" "${mime_type}" >> EICAR_magic_byte_mime_type.csv
done

# Replace the first 4 bytes with "X5O!" in each created file
for file in EICAR_magic_byte_testfile_*; do
    printf 'X5O!' | dd of="${file}" bs=1 count=4 conv=notrunc
done

# Display file information
printf 'Info: File information:\n'
cat "./EICAR_magic_byte_mime_type.csv"

# List all created files with detailed information
printf '\nInfo: Detailed file information:\n'
ls -la "./${eicar_file}" "./EICAR_magic_byte_testfile_"*

# Display the content of each file
printf '\nInfo: File contents:\n'
for file in "${eicar_file}" EICAR_magic_byte_testfile_*; do
    printf '\n%s:\n' "${file}"
    cat "${file}"
done

# Compare files using diff
printf '\nInfo: File comparisons:\n'
for file in EICAR_magic_byte_testfile_*; do
    printf '\nInfo: Comparing eicar.txt with %s:\n' "${file}"
    diff -u "${eicar_file}" "${file}" || true
done

printf '\nInfo: use tool file to show the file type:\n'
file "./EICAR_magic_byte_testfile_"*

mv -v "./EICAR_magic_byte_testfile_"* "${output_dir}"
mv -v "./EICAR_magic_byte_mime_type.csv" "${output_dir}"

cleanup

script_name1="$(basename "${0}")"
printf "\nInfo: script_name1: %s\n" "${script_name1}"
script_path1="$(realpath "$(dirname "${0}")")"
printf "Info: script_path1: %s\n" "${script_path1}"
script_path_with_name="${script_path1}/${script_name1}"
printf "Info: Script path with name: %s\n" "${script_path_with_name}"
printf "Info: Script finished\n"
exit 0
