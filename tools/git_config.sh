#!/usr/bin/env bash
# Author: Pierre Gronau <Pierre.Gronau@ndaal.eu>
# Copyright 2024
# License: All content is licensed under the terms of the <MIT License>
# Developed on: Debian 12.x; macOS Sequoia x86 architecture
# Tested on: Debian 12.x; macOS Sequoia x86 architecture
#
# Exit on error. Append "|| true" if you expect an error.
set -o errexit
# This is equivalent to set -e. It causes the script to exit
# immediately if any command exits with a non-zero status.
#
# Exit on error inside any functions or subshells.
set -o errtrace
# This setting ensures that the ERR trap is inherited by shell functions,
# command substitutions, and commands executed in a subshell environment.
#
# Do not allow use of undefined vars. Use ${VAR:-} to use an undefined VAR
set -o nounset
# This is equivalent to set -u. It treats unset variables as
# an error when substituting.
#
# Catch the error in case mysqldump fails (but gzip succeeds) in `mysqldump |gzip`
# https://vaneyckt.io/posts/safer_bash
set -o pipefail
# This setting causes a pipeline to return the exit status of the last command
# in the pipe that returned a non-zero status.
#
# Turn on traces, useful while debugging but commented out by default
# set -o xtrace

# Set $IFS to only newline and tab.
#
# http://www.dwheeler.com/essays/filenames-in-shell.html
# nosemgrep: ifs-tampering
IFS=$'\n\t'

cleanup() {
    trap - SIGINT SIGTERM ERR EXIT
    printf "%b\n" "\nInfo: Cleanup is running ..."
    # Additional cleanup tasks can be added here if needed
    rm -f -v ./*.tmp
    printf "%b\n" "\nInfo: Cleanup finished ..."
}

trap cleanup SIGINT SIGTERM ERR EXIT

# https://www.shellhacks.com/git-show-config-list-global-local-settings/
git --no-pager config --list
git --no-pager config --list --show-origin

git config --global core.packed.GitLimit "512m" 
git config --global core.GitWindowSize "512m" 
git config --global core.compression "0"

git config --global pack.windowMemory "2047m"
git config --global pack.packSizeLimit "2047m"
git config --global pack.deltaCacheSize "2047m"

git config --global http.sslVersion "tlsv1.2"
git config --global http.sslVerify "false"
git config --global http.maxrequestbuffer "100m"
#git config --global http.postbuffer "150m"
# error HTTP 502 curl 22 The requested URL returned error: 502
git config --global http.postbuffer "500m"
#git config --global http.version "HTTP/1.1"
git config --global http.version "HTTP/2"
git config --global https.sslVerify "false"
git config --global transfer.fsckobjects "true"
git config --global transfer.threads "0"
git config --global merge.renameLimit "62016"
git config --global merge.verifySignatures "false"

# https://coderwall.com/p/tnoiug/rebase-by-default-when-doing-git-pull
git config --global branch.autosetuprebase always

# works only if you have set "${GIT_USER}" and "${GIT_PASSWORD}"
#git remote add origin "https://"${GIT_USER}":"${GIT_PASSWORD}"@dev.azure.com/vf-commonit/WebOperations/_git/vcs-documentation-generator-draft"
git config core.sparsecheckout "true"

cleanup

script_name1="$(basename "${0}")"
printf "\nInfo: script_name1: %s\n" "${script_name1}"
script_path1="$(realpath "$(dirname "${0}")")"
printf "Info: script_path1: %s\n" "${script_path1}"
script_path_with_name="${script_path1}/${script_name1}"
printf "Info: Script path with name: %s\n" "${script_path_with_name}"
printf "Info: Script finished\n"
exit 0
