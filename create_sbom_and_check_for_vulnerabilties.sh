#!/usr/bin/env bash
# Author: Pierre Gronau <Pierre.Gronau@ndaal.eu>
# Copyright 2024,2025
# License: All content is licensed under the terms of the <Apache 2.0>
# Developed on: Debian 12.x; macOS Sequoia x86 architecture
# Tested on: Debian 12.x; macOS Sequoia x86 architecture

set -o errexit
set -o errtrace
set -o nounset
set -o pipefail
# set -o xtrace

# nosemgrep: ifs-tampering
IFS=$'\n\t'

# Error trapping function
# Add this line before the error_handler function
# shellcheck disable=SC2317
error_handler() {
    local line_number="${1}"
    local error_code="${2}"
    local last_command="${BASH_COMMAND}"
    printf "Error: Error occurred in line %s (error code: %s)\n" "${line_number}" "${error_code}"
    printf "Error: Failed command: %s\n" "${last_command}"

    # Success case
    return 0
}

# Set the error trap
trap "error_handler ${LINENO} \$?" ERR

cleanup() {
    trap - SIGINT SIGTERM ERR EXIT
    printf "%b\n" "\nInfo: Cleanup is running ..."
    # Additional cleanup tasks can be added here if needed
    rm -f -v ./*.tmp
    printf "%b\n" "\nInfo: Cleanup finished ..."

    # Success case
    return 0
}

trap cleanup SIGINT SIGTERM EXIT

DIRDATE="$(date +"%Y-%m-%d")"
readonly DIRDATE
printf "Info: Current date: %s\n\n" "${DIRDATE}"

check_command() {
    if ! command -v "${1}" &>/dev/null; then
        printf "Error: %s is not installed or not in PATH. Please install it and try again.\n" "${1}"
        return 1
    fi

    # Success case
    return 0
}

check_command "syft" || exit 1
check_command "grype" || exit 1
check_command "trivy" || exit 1

if [ $# -eq 0 ]; then
    printf "Info: Please provide the path to the repository.\n"
    printf "Info: Usage: %s /path/to/repository\n" "${0}"
    exit 1
fi

REPO_PATH="${1}"
readonly REPO_PATH
printf "Info: Current REPO_PATH: %s\n\n" "${REPO_PATH}"

RESULTS_FILE="sbom_scan_results_${DIRDATE}.txt"
readonly RESULTS_FILE
printf "Info: Current RESULTS_FILE: %s\n\n" "${RESULTS_FILE}"

DEFAULT_REQUIRED_SPACE_MB=1000  # Default: 1GB in MB
readonly DEFAULT_REQUIRED_SPACE_MB
printf "Info: Current DEFAULT_REQUIRED_SPACE_MB: %s\n\n" "${DEFAULT_REQUIRED_SPACE_MB}"

# Use configured or default values
REQUIRED_SPACE_MB="${REQUIRED_SPACE_MB:-${DEFAULT_REQUIRED_SPACE_MB}}"
readonly REQUIRED_SPACE_MB
printf "Info: Current REQUIRED_SPACE_MB: %s\n\n" "${REQUIRED_SPACE_MB}"

# Convert MB to KB for df comparison
REQUIRED_SPACE_KB=$((REQUIRED_SPACE_MB * 1024))
readonly REQUIRED_SPACE_KB
printf "Info: Current REQUIRED_SPACE_KB: %s\n\n" "${REQUIRED_SPACE_KB}"

printf "Info: Generating SBOM...\n"
export SYFT_FILE_METADATA_SELECTION="all"

# Enhanced disk space checking function
check_disk_space() {
    local available_space
    local required_space="${REQUIRED_SPACE_KB}"

    available_space=$(df -k . | awk 'NR==2 {print $4}')

    printf "Info: Checking disk space. Required: %sMB (%sKB), Available: %sMB\n" \
        "${REQUIRED_SPACE_MB}" "${required_space}" "$((available_space / 1024))"

    if [ "${available_space}" -lt "${required_space}" ]; then
        printf "Error: Insufficient disk space!\n"
        printf "Error: Required: %sMB (%sKB)\n" "${REQUIRED_SPACE_MB}" "${required_space}"
        printf "Error: Available: %sMB (%sKB)\n" "$((available_space / 1024))" "${available_space}"
        exit 1
    fi

    # If we have less than 2x the required space, warn the user
    if [ "${available_space}" -lt "$((required_space * 2))" ]; then
        printf "Info: Warning: Available disk space is less than 2x the required space\n"
        printf "Info: This might cause issues if large files are generated\n"
    fi
}

check_disk_space

generate_sbom() {
    local output_format="${1}"
    local output_file="${2}"
    printf "Info: Generating SBOM in %s format...\n" "${output_format}"
    syft dir:"${REPO_PATH}" --output "${output_format}" \
        --exclude "**/sbom.json" \
        --exclude "**/sbom_spdx-tag-value_2.3.spdx" \
        --exclude "**/sbom_spdx-tag-value_2.2.spdx" \
        --exclude "**/sbom_spdx-json_2.3.json" \
        --exclude "**/sbom_spdx-json_2.2.json" \
        --exclude "**/sbom_cyclonedx-json.json" \
        --exclude "**/sbom_syft-json.json" \
        --exclude "**/sbom_syft-text.txt" \
        --exclude "**/sbom_syft-table.table" \
        > "${output_file}" || {
            printf "Error: Failed to generate SBOM for %s.\n" "${output_file}"
            exit 1
        }
}

declare -A sbom_formats=(
    ["spdx-json"]="sbom.json"
    ["spdx-tag-value"]="sbom_spdx-tag-value_2.3.spdx"
    ["spdx-tag-value@2.2"]="sbom_spdx-tag-value_2.2.spdx"
    ["spdx-json"]="sbom_spdx-json_2.3.json"
    ["spdx-json@2.2"]="sbom_spdx-json_2.2.json"
    ["cyclonedx-json"]="sbom_cyclonedx-json.json"
    ["syft-json"]="sbom_syft-json.json"
    ["syft-text"]="sbom_syft-text.txt"
    ["syft-table"]="sbom_syft-table.table"
    ["spdx-json"]="sbom.json"
)

# Remove all destination files before creating new ones
for file in "${sbom_formats[@]}"; do
    rm -f -v "${file}"
done

for format in "${!sbom_formats[@]}"; do
    generate_sbom "${format}" "${sbom_formats[${format}]}"
done

# Check if sbom.json exists and has content
if [ ! -f ./sbom.json ] || [ ! -s ./sbom.json ]; then
    printf "Error: sbom.json is empty or doesn't exist. Regenerating...\n"
    generate_sbom "syft-json" "sbom.json"
fi

line_count=$(wc -l < ./sbom.json)

#if [ "$line_count" -le 1 ]; then
#    printf "Error: ./sbom.json contains 1 or fewer lines. Exiting.\n"
#    exit 1
#fi

# Verify content again
if [ ! -s ./sbom.json ]; then
    printf "Error: Failed to generate sbom.json with content. Exiting.\n"
    exit 1
fi

printf "Info: ./sbom.json exists and contains more than 1 lines. Proceeding.\n"

printf "Info: SBOM generated successfully with name sbom.json.\n"

rm -f -v "${RESULTS_FILE}"

printf "Info: Scanning for vulnerabilities with Grype...\n"
{
    printf "Info: === Grype Scan Results ===\n\n"
    if ! grype sbom:./sbom.json; then
        printf "Error: Failed to scan for vulnerabilities with Grype.\n"
        exit 1
    fi
    printf "\n\n"
} | tee -a "${RESULTS_FILE}"

printf "Info: Scanning for vulnerabilities with Trivy...\n"
{
    printf "Info: === Trivy Scan Results ===\n\n"

    TRIVY_CACHE_DIR="${HOME}/.cache/trivy"
    mkdir -p -v "${TRIVY_CACHE_DIR}"

    if [ -f "${TRIVY_CACHE_DIR}/trivy.db" ] && [ "$(find "${TRIVY_CACHE_DIR}/trivy.db" -mtime -1)" ]; then
        printf "Info: Using existing Trivy database\n"
    else
        printf "Info: Updating Trivy database...\n"
        for i in {1..3}; do
            if trivy --cache-dir "${TRIVY_CACHE_DIR}" image --download-db-only; then
                printf "Info: Database updated successfully\n"
                break
            fi
            printf "Error: Database update failed, retrying in %d seconds...\n" $((2**i))
            sleep $((2**i))
        done
    fi

    trivy_output=$(trivy --cache-dir "${TRIVY_CACHE_DIR}" fs -v --scanners vuln,misconfig,secret "${REPO_PATH}")
    if [ -z "${trivy_output}" ]; then
        printf "Info: No vulnerabilities found by Trivy.\n"
    else
        printf "%s\n" "${trivy_output}"
    fi
    printf "\n\n"

} | tee -a "${RESULTS_FILE}"

printf "Info: Vulnerability scans completed. Results saved to %s\n" "${RESULTS_FILE}"
printf "Info: Scan results are available in %s\n" "${RESULTS_FILE}"

printf "Info: Script completed successfully.\n"

cleanup

script_name1="$(basename "${0}")"
printf "\nInfo: script_name1: %s\n" "${script_name1}"
script_path1="$(realpath "$(dirname "${0}")")"
printf "Info: script_path1: %s\n" "${script_path1}"
script_path_with_name="${script_path1}/${script_name1}"
printf "Info: Script path with name: %s\n" "${script_path_with_name}"
printf "Info: Script finished\n"
exit 0
