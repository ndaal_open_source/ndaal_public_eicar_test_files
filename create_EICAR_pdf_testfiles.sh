#!/usr/bin/env bash
# Author: Pierre Gronau <Pierre.Gronau@ndaal.eu>
# Copyright 2024, 2025
# License: All content is licensed under the terms of the <MIT License>
# Developed on: Debian 12.8x; macOS Sequoia x86 architecture
# Tested on: Debian 12.8x; macOS Sequoia x86 architecture
#
# Note
# This script creates or download files containing the EICAR test string,
# which may trigger antivirus software. Use with caution and only
# in controlled environments for legitimate testing purposes.
#
# Exit on error. Append "|| true" if you expect an error.
set -o errexit
# This is equivalent to set -e. It causes the script to exit
# immediately if any command exits with a non-zero status.
#
# Exit on error inside any functions or subshells.
set -o errtrace
# This setting ensures that the ERR trap is inherited by shell functions,
# command substitutions, and commands executed in a subshell environment.
#
# Do not allow use of undefined vars. Use ${VAR:-} to use an undefined VAR
set -o nounset
# This is equivalent to set -u. It treats unset variables as
# an error when substituting.
#
# Catch the error in case mysqldump fails (but gzip succeeds) in `mysqldump |gzip`
# https://vaneyckt.io/posts/safer_bash
set -o pipefail
# This setting causes a pipeline to return the exit status of the last command
# in the pipe that returned a non-zero status.
#
# Turn on traces, useful while debugging but commented out by default
# set -o xtrace

# Set $IFS to only newline and tab.
#
# http://www.dwheeler.com/essays/filenames-in-shell.html
# nosemgrep: ifs-tampering
IFS=$'\n\t'

# Error trapping function
# Add this line before the error_handler function
# shellcheck disable=SC2317
error_handler() {
    local line_number="${1}"
    local error_code="${2}"
    local last_command="${BASH_COMMAND}"
    printf "Error: Error occurred in line %s (error code: %s)\n" "${line_number}" "${error_code}"
    printf "Error: Failed command: %s\n" "${last_command}"

    # Success case
    return 0
}

# Set the error trap
trap "error_handler ${LINENO} \$?" ERR

cleanup() {
    trap - SIGINT SIGTERM ERR EXIT
    printf "%b\n" "\nInfo: Cleanup is running ..."
    # Additional cleanup tasks can be added here if needed
    rm -f -v ./*.tmp
    printf "%b\n" "\nInfo: Cleanup finished ..."
}

trap cleanup SIGINT SIGTERM EXIT

# Define the EICAR test string
# shellcheck disable=SC2016
input_string='X5O!P%@AP[4\PZX54(P^)7CC)7}$EICAR-STANDARD-ANTIVIRUS-TEST-FILE!$H+H*'
readonly input_string

# Create a text file with the EICAR test string
printf "%s\n" "${input_string}" > eicar.txt

printf "precautions\n\nThis is an EICAR test file\nwith the following EICAR Test string:\n\n%s\n" "${input_string}" > eicar2.txt

# Function to create a PDF with a specified version
generate_pdf_version() {
    local version="${1}"
    local output="${2}"

    printf "Info: Creating PDF version %s: %s\n" "${version}" "${output}"
    gs -dBATCH -dNOPAUSE -q -sDEVICE=pdfwrite -dCompatibilityLevel="${version}" -sOutputFile="${output}" -c "showpage"
}

# Function to attach a file to a PDF
attach_file_to_pdf() {
    local input_pdf="${1}"
    local attachment="${2}"
    local output_pdf="${3}"

    printf "Info: Attaching %s to %s, creating %s\n" "${attachment}" "${input_pdf}" "${output_pdf}"
    pdftk "${input_pdf}" attach_files "${attachment}" output "${output_pdf}"
}

# Generate PDFs with different versions, with and without attachments
for version in "1.0" "1.1" "1.2" "1.3" "1.4" "1.5" "1.6" "1.7" "2.0"; do
    base_output="pdf_version_${version}.pdf"
    generate_pdf_version "${version}" "${base_output}"

    # Create a variant with an embedded attachment
    attached_output="pdf_version_${version}_with_attachment.pdf"
    attach_file_to_pdf "${base_output}" "eicar.txt" "${attached_output}"
done

printf "\nInfo: Processing complete.\n"

cleanup

script_name1="$(basename "${0}")"
printf "\nInfo: script_name1: %s\n" "${script_name1}"
script_path1="$(realpath "$(dirname "${0}")")"
printf "Info: script_path1: %s\n" "${script_path1}"
script_path_with_name="${script_path1}/${script_name1}"
printf "Info: Script path with name: %s\n" "${script_path_with_name}"
printf "Info: Script finished\n"
exit 0
