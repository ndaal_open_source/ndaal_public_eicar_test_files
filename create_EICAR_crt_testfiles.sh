#!/usr/bin/env bash
# Author: Pierre Gronau <Pierre.Gronau@ndaal.eu>
# Copyright 2025
# License: All content is licensed under the terms of the <Apache 2.0>
# Developed on: Debian 12.9x; macOS Sequoia x86 architecture
# Tested on: Debian 12.9x; macOS Sequoia x86 architecture
#
set -o errexit
set -o errtrace
set -o nounset
set -o pipefail
# set -o xtrace

# nosemgrep: ifs-tampering
IFS=$'\n\t'

# Error trapping function
# Add this line before the error_handler function
# shellcheck disable=SC2317
error_handler() {
    local line_number="${1}"
    local error_code="${2}"
    local last_command="${BASH_COMMAND}"
    printf "Error: Error occurred in line %s (error code: %s)\n" "${line_number}" "${error_code}"
    printf "Error: Failed command: %s\n" "${last_command}"
}

# Set the error trap
trap 'error_handler ${LINENO} $?' ERR

cleanup() {
    trap - SIGINT SIGTERM ERR EXIT
    printf "%b\n" "\nInfo: Cleanup is running ..."
    # Additional cleanup tasks can be added here if needed
    rm -f -v ./*.tmp
    printf "%b\n" "\nInfo: Cleanup finished ..."
}

trap cleanup SIGINT SIGTERM ERR EXIT

# Define the EICAR test string
input_string='X5O!P%@AP[4\PZX54(P^)7CC)7}$EICAR-STANDARD-ANTIVIRUS-TEST-FILE!$H+H*'
readonly input_string

# Define the EICAR file name
eicar_file="eicar.txt"
readonly eicar_file

# Create a text file with the EICAR test string and ensure it ends with a newline
printf "%s\n" "${input_string}" > "${eicar_file}"

if [ ! -f "${eicar_file}" ]; then
    printf "Error: %s not found. Exiting.\n" "${eicar_file}"
    exit 1
fi

# Define output directory
output_dir="./dataset/EICAR_crt_testfiles"
readonly output_dir
printf "Info: Current output_dir: %s\n" "${output_dir}"

# Create output directory if it doesn't exist
if [ ! -d "${output_dir}" ]; then
    if mkdir "${output_dir}"; then
        printf "Info: Created output directory: %s\n" "${output_dir}"
    else
        printf "Error: Failed to create output directory: %s\n" "${output_dir}" >&2
        exit 1
    fi
else
    printf "Info: Output directory already exists: %s\n" "${output_dir}"
fi

# Create the EICAR test file with the original name
cp -p -v "${eicar_file}" "${output_dir}/${eicar_file}_only_EICAR.crt"
printf "Info: Created file: %s\n" "${output_dir}/${eicar_file}_only_EICAR.crt"

# Create the EICAR test file with embedded certificate format
cert_file="${output_dir}/${eicar_file}_with_EICAR_and_signature_text.crt"
readonly cert_file
printf "Info: Current cert_file: %s\n" "${cert_file}"

# Create the certificate file with the EICAR test string
{
    printf "%s\n" "-----BEGIN CERTIFICATE-----"
    printf "%s\n" "${input_string}"
    printf "%s\n" "-----END CERTIFICATE-----"
} > "${cert_file}"

# Create the EICAR test file with embedded certificate format
cert_file2="${output_dir}/${eicar_file}_with_EICAR_and_signature_text_without_new_lines.crt"
readonly cert_file2
printf "Info: Current cert_file: %s\n" "${cert_file}"

printf "Info: Created file: %s\n" "${cert_file}"

# Create the certificate file with the EICAR test string
{
    printf "%s" "-----BEGIN CERTIFICATE-----"
    printf "%s" "${input_string}"
    printf "%s" "-----END CERTIFICATE-----"
} > "${cert_file2}"

printf "Info: Created file: %s\n" "${cert_file2}"

# Optional: Display the contents of the certificate file
cat "${cert_file}"
cat "${cert_file2}"

printf "\nInfo: Processing complete.\n"

cleanup

script_name1="$(basename "${0}")"
printf "\nInfo: script_name1: %s\n" "${script_name1}"
script_path1="$(realpath "$(dirname "${0}")")"
printf "Info: script_path1: %s\n" "${script_path1}"
script_path_with_name="${script_path1}/${script_name1}"
printf "Info: Script path with name: %s\n" "${script_path_with_name}"
printf "Info: Script finished\n"
exit 0
